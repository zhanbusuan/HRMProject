package com.hrm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class Deptservice11001Application {

    public static void main(String[] args) {
        SpringApplication.run(Deptservice11001Application.class, args);
    }

}

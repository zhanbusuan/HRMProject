package com.hrm.biz.impl;

import com.hrm.biz.DeptBiz;
import com.hrm.dao.DeptDao;
import com.hrm.entity.HRDept;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: lsy
 * \* Date: 2020/4/3 0003
 * \* Time: 15:41
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@Service
public class DeptBizImpl implements DeptBiz {
    @Resource
    private DeptDao deptDao;
    @Override
    public HRDept getbyid(int id) {
        return deptDao.getbyid(id);
    }

    @Override
    public boolean add(HRDept hrDept) {
        if (deptDao.getbydname(hrDept.getDname())==null){
            return deptDao.add(hrDept);
        }
        return false;
    }

    @Override
    public boolean update(HRDept hrDept) {
        if (deptDao.getbydname(hrDept.getDname())==null){
            return deptDao.update(hrDept);
        }
        return false;
    }

    @Override
    public boolean delete(int id) {
        if (deptDao.getbyid(id)!=null){
            return deptDao.delete(id);
        }
        return false;
    }

    @Override
    public List<HRDept> getDeptList() {
        return deptDao.getDeptList();
    }

    @Override
    public HRDept getbydname(String dname) {
        return deptDao.getbydname(dname);
    }
}

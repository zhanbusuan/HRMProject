package com.hrm.dao;

import com.hrm.entity.HRDept;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: lsy
 * \* Date: 2020/4/3 0003
 * \* Time: 14:47
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@Mapper
public interface DeptDao {
    public HRDept getbyid(int id);
    public boolean add(HRDept hrDept);
    public boolean update(HRDept hrDept);
    public boolean delete(int id);
    public List<HRDept> getDeptList();
    public HRDept getbydname(String dname);
}

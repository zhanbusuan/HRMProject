package com.hrm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class Deptservice11002Application {

    public static void main(String[] args) {
        SpringApplication.run(Deptservice11002Application.class, args);
    }

}

package com.hrm;

import com.github.pagehelper.PageInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: zt
 * \* Date: 2020/3/23
 * \* Time: 20:02
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@Configuration
public class MyConfiguration {

    @Bean
    public PageInterceptor pageHelper(){
        PageInterceptor pegeInterceptor = new PageInterceptor();
        Properties properties = new Properties();
        properties.setProperty("helperDialect", "mysql");  //数据库
        properties.setProperty("reasonable", "true");
        properties.setProperty("supportMethodsArguments", "true");
        properties.setProperty("autoRuntimeDialect", "true");
        pegeInterceptor.setProperties(properties);
        return pegeInterceptor;
    }
}
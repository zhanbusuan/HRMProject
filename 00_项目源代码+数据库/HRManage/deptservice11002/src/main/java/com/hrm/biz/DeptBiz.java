package com.hrm.biz;

import com.github.pagehelper.PageInfo;
import com.hrm.entity.HRDept;

import java.util.List;

public interface DeptBiz {
    public HRDept getbyid(int id);
    public boolean add(HRDept hrDept);
    public boolean update(HRDept hrDept);
    public boolean delete(int id);
    public List<HRDept> getDeptList();
    public PageInfo<HRDept> queryByPage(int pagenum);
}

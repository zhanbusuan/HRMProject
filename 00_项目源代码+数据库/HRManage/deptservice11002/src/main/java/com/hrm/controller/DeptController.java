package com.hrm.controller;

import com.github.pagehelper.PageInfo;
import com.hrm.biz.DeptBiz;
import com.hrm.entity.HRDept;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: lsy
 * \* Date: 2020/4/3 0003
 * \* Time: 16:00
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@RestController
@RequestMapping("dept")
public class DeptController {
    @Resource
    private DeptBiz deptBiz;
    @RequestMapping("getbyid")
    public HRDept getbyid(@RequestParam int id){
        return deptBiz.getbyid(id);
    }
    @RequestMapping("add")
    public boolean add(@RequestBody HRDept hrDept){
        return deptBiz.add(hrDept);
    }
    @RequestMapping("update")
    public boolean update(@RequestBody HRDept hrDept){
        return deptBiz.update(hrDept);
    }
    @RequestMapping("delete")
    public boolean delete(@RequestParam int id){
        return deptBiz.delete(id);
    }
    @GetMapping("getdeptlist")
    public List<HRDept> getdeptList(){
        return deptBiz.getDeptList();
    }
    @RequestMapping("querydeptbypage")
    public PageInfo<HRDept> queryDeptByPage(@RequestParam int pagenum){
        return deptBiz.queryByPage(pagenum);
    }
}

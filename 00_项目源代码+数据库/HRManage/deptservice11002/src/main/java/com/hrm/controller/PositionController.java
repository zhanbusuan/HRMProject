package com.hrm.controller;

import com.github.pagehelper.PageInfo;
import com.hrm.biz.PositionBiz;
import com.hrm.entity.HRPostition;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: lsy
 * \* Date: 2020/4/3 0003
 * \* Time: 16:00
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@RestController
@RequestMapping("position")
public class PositionController {
    @Resource
    private PositionBiz positionBiz;
    @RequestMapping("getPosibyid")
    public HRPostition getbyid(@RequestParam int id){
        return positionBiz.getbyid(id);
    }
    @RequestMapping("add")
    public boolean addPosi(@RequestBody HRPostition hrPostition){
        return positionBiz.add(hrPostition);
    }
    @RequestMapping("update")
    public boolean updatePosi(@RequestBody HRPostition hrPostition){
        return positionBiz.update(hrPostition);
    }
    @RequestMapping("delete")
    public boolean deletePosi(@RequestParam int id){
        return positionBiz.delete(id);
    }
    @GetMapping("getpositionlist")
    public List<HRPostition> getpositionList(){
        return positionBiz.getPostitionList();
    }
    @RequestMapping("querypostitionbypage")
    public PageInfo<HRPostition> queryPostitionByPage(@RequestParam int pagenum){
        return positionBiz.queryByPage(pagenum);
    }
}

package com.hrm.dao;

import com.hrm.entity.HRPostition;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PositionDao {
    public HRPostition getbyid(int id);
    public boolean add(HRPostition hrPostition);
    public boolean update(HRPostition hrPostition);
    public boolean delete(int id);
    public List<HRPostition> getPostitionList();
    public HRPostition getbypname(String pname);
}

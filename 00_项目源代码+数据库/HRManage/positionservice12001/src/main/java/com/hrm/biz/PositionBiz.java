package com.hrm.biz;


import com.hrm.entity.HRPostition;

import java.util.List;

public interface PositionBiz {
    public HRPostition getbyid(int id);
    public boolean add(HRPostition hrPostition);
    public boolean update(HRPostition hrPostition);
    public boolean delete(int id);
    public List<HRPostition> getPostitionList();
    public HRPostition getbypname(String pname);
}

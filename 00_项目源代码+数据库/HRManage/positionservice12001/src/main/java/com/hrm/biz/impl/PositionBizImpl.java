package com.hrm.biz.impl;

import com.hrm.biz.PositionBiz;
import com.hrm.dao.PositionDao;
import com.hrm.entity.HRDept;
import com.hrm.entity.HRPostition;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: lsy
 * \* Date: 2020/4/3 0003
 * \* Time: 15:41
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@Service
public class PositionBizImpl implements PositionBiz {
    @Resource
    private PositionDao positionDao;
    @Override
    public HRPostition getbyid(int id) {
        return positionDao.getbyid(id);
    }

    @Override
    public boolean add(HRPostition hrPostition) {
        if (positionDao.getbypname(hrPostition.getPname())==null){
            return positionDao.add(hrPostition);
        }
        return false;
    }

    @Override
    public boolean update(HRPostition hrPostition) {
        if (positionDao.getbypname(hrPostition.getPname())==null){
            return positionDao.update(hrPostition);
        }
        return false;
    }

    @Override
    public boolean delete(int id) {
        if (positionDao.getbyid(id)!=null){
            return positionDao.delete(id);
        }
        return false;
    }

    @Override
    public List<HRPostition> getPostitionList() {
        return positionDao.getPostitionList();
    }

    @Override
    public HRPostition getbypname(String pname) {
        return positionDao.getbypname(pname);
    }
}

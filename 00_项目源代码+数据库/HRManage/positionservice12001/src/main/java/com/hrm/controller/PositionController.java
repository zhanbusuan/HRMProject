package com.hrm.controller;

import com.hrm.biz.PositionBiz;
import com.hrm.entity.HRDept;
import com.hrm.entity.HRPostition;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: lsy
 * \* Date: 2020/4/3 0003
 * \* Time: 16:00
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@RestController
@RequestMapping("position")
public class PositionController {
    @Resource
    private PositionBiz positionBiz;
    @RequestMapping("getbyid")
    public HRPostition getbyid(@RequestParam int id){
        return positionBiz.getbyid(id);
    }
    @RequestMapping("add")
    public boolean add(HRPostition hrPostition){
        return positionBiz.add(hrPostition);
    }
    @RequestMapping("update")
    public boolean update(HRPostition hrPostition){
        return positionBiz.update(hrPostition);
    }
    @RequestMapping("delete")
    public boolean delete(int id){
        return positionBiz.delete(id);
    }
    @GetMapping("getpositionlist")
    public List<HRPostition> getpositionList(){
        return positionBiz.getPostitionList();
    }
}

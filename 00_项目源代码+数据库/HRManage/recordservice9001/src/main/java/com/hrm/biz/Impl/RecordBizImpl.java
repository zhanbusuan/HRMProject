package com.hrm.biz.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hrm.biz.RecordBiz;
import com.hrm.dao.RecordMapper;
import com.hrm.entity.HRRecord;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Service
public class RecordBizImpl implements RecordBiz {

    @Resource
    private RecordMapper mapper;
    public PageInfo<HRRecord> selectByParam(int pagenum,HRRecord record){
        PageHelper.startPage(pagenum,6);
        List<HRRecord> list = mapper.selectByP(record);
        for (HRRecord R:list){

            System.out.println(R);
        }
        PageInfo<HRRecord> page = new PageInfo<HRRecord>(list);
        System.out.println("总数量：" + page.getTotal());
        System.out.println("当前页查询记录：" + page.getList().size());
        System.out.println("当前页码：" + page.getPageNum());
        System.out.println("每页显示数量：" + page.getPageSize());
        System.out.println("总页：" + page.getPages());
        System.out.println(page);
        return page;
    }

    @Override
    public PageInfo<HRRecord> selectAll(int pagenum) {
        PageHelper.startPage(pagenum,3);
        List<HRRecord> list = mapper.selectAll();
        PageInfo<HRRecord> page = new PageInfo<HRRecord>(list);
        return page;
    }

    @Override
    public HRRecord selectByRid(int rid) {
        return mapper.selectByRid(rid);
    }

    @Override
    public List<HRRecord> selectByUid(int uid) {
        return mapper.selectByUid(uid);
    }


    @Override
    public boolean addRecord(HRRecord record) {
        return mapper.addRecord(record);
    }

    @Override
    public boolean updateRecord(HRRecord record) {
        return mapper.updateRecord(record);
    }

    @Override
    public boolean deleteRecord(int rid) {
        return mapper.deleteRecord(rid);
    }
}

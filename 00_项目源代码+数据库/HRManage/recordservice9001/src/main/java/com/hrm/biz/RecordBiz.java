package com.hrm.biz;

import com.github.pagehelper.PageInfo;
import com.hrm.entity.HRRecord;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

public interface RecordBiz {
    public PageInfo<HRRecord> selectByParam(int pagenum,HRRecord record);
    public PageInfo<HRRecord> selectAll(int pagenum);
    public HRRecord selectByRid(int rid);
    public List<HRRecord> selectByUid(int uid);
    public boolean addRecord(HRRecord record);
    public boolean updateRecord(HRRecord record);
    public boolean deleteRecord(int  rid);
}

package com.hrm.controller;

import com.github.pagehelper.PageInfo;
import com.hrm.biz.RecordBiz;
import com.hrm.entity.HRRecord;
import com.hrm.entity.HRUser;
import com.hrm.feigns.UserFeign;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: zt
 * \* Date: 2020/4/3
 * \* Time: 17:09
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@RestController
@RequestMapping("myrecord")
public class RecordController {
    @Resource
    private RecordBiz recordBiz;
    @Resource
    private UserFeign userFeign;



    @RequestMapping("list")
    public PageInfo<HRRecord> list(int pagenum){
        PageInfo<HRRecord> page =recordBiz.selectAll(pagenum);
       /* for(HRRecord record:list){
            int uid = record.getUid();
            record.setUser(userFeign.queryUser());
            record.setOdept();
            record.setOpostition();
            record.setNdept();
            record.setNpostition();

        }*/
        return page;
    }


    @RequestMapping("listByParam")
    public PageInfo<HRRecord> list(int pagenum,@RequestBody HRRecord record){
        PageInfo<HRRecord> page =recordBiz.selectByParam(pagenum,record);
        return page;
    }
    @RequestMapping("getOne")
    public HRRecord getOne(int rid ){
        return recordBiz.selectByRid(rid);
    }

    @RequestMapping("update")
    public boolean update(@RequestBody HRRecord record ){
        return recordBiz.updateRecord(record);
    }

    @RequestMapping("add")
    public boolean addrecord(@RequestBody  HRRecord record){
        System.out.println("9001:record                 addrecord"+record);
        System.out.println(record);
        return recordBiz.addRecord(record);
    }

    @RequestMapping("delete")
    public boolean deleterecord(int rid){
        return recordBiz.deleteRecord(rid);
    }
}
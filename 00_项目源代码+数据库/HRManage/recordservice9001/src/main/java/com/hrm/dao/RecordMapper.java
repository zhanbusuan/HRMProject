package com.hrm.dao;

import com.hrm.entity.HRRecord;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;
import java.util.List;
@Mapper
public interface RecordMapper {
    public List<HRRecord> selectAll();
    public HRRecord selectByRid(int rid);
    public List<HRRecord> selectByUid(int uid);
    public List<HRRecord> selectByP(HRRecord record);
    public boolean addRecord(HRRecord record);
    public boolean updateRecord(HRRecord record);
    public boolean deleteRecord(int rid);

}

package com.hrm.feigns;

import com.hrm.entity.HRUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: zt
 * \* Date: 2020/4/6
 * \* Time: 13:21
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@FeignClient(name = "userservice")
public interface UserFeign {
    @GetMapping("queryAllHRUser")
    @ResponseBody
    public List<HRUser> queryAllHRUser();
    @PostMapping("updateHRUser")
    @ResponseBody
    public boolean updateHRUser(@RequestBody HRUser hrUser);
    @PostMapping("insertHRUser")
    @ResponseBody
    public boolean insertHRUser(@RequestBody HRUser hrUser);
    @PostMapping("deleteHRUserByUid")
    @ResponseBody
    public boolean deleteHRUserByUid(@RequestParam int uid);
    @RequestMapping("queryUser")
    public String queryUser(Model model);
}
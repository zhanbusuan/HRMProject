package com.hrm.entity;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: WYJ
 * \* Date: 2020/4/3
 * \* Time: 11:27
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
public class HRDept {
    private int deptid;
    private String dname;

    public int getDeptid() {
        return deptid;
    }

    public void setDeptid(int deptid) {
        this.deptid = deptid;
    }

    public String getDname() {
        return dname;
    }

    public void setDname(String dname) {
        this.dname = dname;
    }

    public HRDept() {
    }

    public HRDept(int deptid, String dname) {
        this.deptid = deptid;
        this.dname = dname;
    }

    @Override
    public String toString() {
        return "HRDept{" +
                "deptid=" + deptid +
                ", dname='" + dname + '\'' +
                '}';
    }
}
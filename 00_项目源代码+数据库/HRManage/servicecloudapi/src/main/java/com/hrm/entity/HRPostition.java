package com.hrm.entity;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: WYJ
 * \* Date: 2020/4/3
 * \* Time: 11:28
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
public class HRPostition {
    private int pid;
    private String pname;

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public HRPostition() {
    }

    public HRPostition(int pid, String pname) {
        this.pid = pid;
        this.pname = pname;
    }

    @Override
    public String toString() {
        return "HRPostition{" +
                "pid=" + pid +
                ", pname='" + pname + '\'' +
                '}';
    }
}
package com.hrm.entity;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: zt
 * \* Date: 2020/4/7
 * \* Time: 19:57
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
public class HRRecord {
    private int rid;
    private int uid;
    private String uname;
    private HRUser user;
    private int odeptid;
    private HRDept odept;
    private int ndeptid;
    private HRDept ndept;
    private int opoisid;
    private HRPostition opostition;
    private int npoisid;
    private HRPostition npostition;
    private String changetime;

    public int getRid() {
        return rid;
    }

    public void setRid(int rid) {
        this.rid = rid;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public HRUser getUser() {
        return user;
    }

    public void setUser(HRUser user) {
        this.user = user;
    }

    public int getOdeptid() {
        return odeptid;
    }

    public void setOdeptid(int odeptid) {
        this.odeptid = odeptid;
    }

    public HRDept getOdept() {
        return odept;
    }

    public void setOdept(HRDept odept) {
        this.odept = odept;
    }

    public int getNdeptid() {
        return ndeptid;
    }

    public void setNdeptid(int ndeptid) {
        this.ndeptid = ndeptid;
    }

    public HRDept getNdept() {
        return ndept;
    }

    public void setNdept(HRDept ndept) {
        this.ndept = ndept;
    }

    public int getOpoisid() {
        return opoisid;
    }

    public void setOpoisid(int opoisid) {
        this.opoisid = opoisid;
    }

    public HRPostition getOpostition() {
        return opostition;
    }

    public void setOpostition(HRPostition opostition) {
        this.opostition = opostition;
    }

    public int getNpoisid() {
        return npoisid;
    }

    public void setNpoisid(int npoisid) {
        this.npoisid = npoisid;
    }

    public HRPostition getNpostition() {
        return npostition;
    }

    public void setNpostition(HRPostition npostition) {
        this.npostition = npostition;
    }

    public String getChangetime() {
        return changetime;
    }

    public void setChangetime(String changetime) {
        this.changetime = changetime;
    }

    public HRRecord() {
    }

    @Override
    public String toString() {
        return "HRRecord{" +
                "rid=" + rid +
                ", uid=" + uid +
                ", uname='" + uname + '\'' +
                ", user=" + user +
                ", odeptid=" + odeptid +
                ", odept=" + odept +
                ", ndeptid=" + ndeptid +
                ", ndept=" + ndept +
                ", opoisid=" + opoisid +
                ", opostition=" + opostition +
                ", npoisid=" + npoisid +
                ", npostition=" + npostition +
                ", changetime='" + changetime + '\'' +
                '}';
    }
}
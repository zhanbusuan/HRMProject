package com.hrm.entity;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: zt
 * \* Date: 2020/4/3
 * \* Time: 0:17
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
public class HRUser {
    private int uid;
    private String uname;
    private String upwd;
    private String usex;
    private int uage;
    private String uedu;
    private int udeptid;
    private HRDept hrDept;
    private int upostid;
    private HRPostition hrPostition;
    private String umobile;
    private String uintime;
    private String uemail;
    private String ustatus;

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getUpwd() {
        return upwd;
    }

    public void setUpwd(String upwd) {
        this.upwd = upwd;
    }

    public String getUsex() {
        return usex;
    }

    public void setUsex(String usex) {
        this.usex = usex;
    }

    public int getUage() {
        return uage;
    }

    public void setUage(int uage) {
        this.uage = uage;
    }

    public String getUedu() {
        return uedu;
    }

    public void setUedu(String uedu) {
        this.uedu = uedu;
    }

    public int getUdeptid() {
        return udeptid;
    }

    public void setUdeptid(int udeptid) {
        this.udeptid = udeptid;
    }

    public HRDept getHrDept() {
        return hrDept;
    }

    public void setHrDept(HRDept hrDept) {
        this.hrDept = hrDept;
    }

    public int getUpostid() {
        return upostid;
    }

    public void setUpostid(int upostid) {
        this.upostid = upostid;
    }

    public HRPostition getHrPostition() {
        return hrPostition;
    }

    public void setHrPostition(HRPostition hrPostition) {
        this.hrPostition = hrPostition;
    }

    public String getUmobile() {
        return umobile;
    }

    public void setUmobile(String umobile) {
        this.umobile = umobile;
    }

    public String getUintime() {
        return uintime;
    }

    public void setUintime(String uintime) {
        this.uintime = uintime;
    }

    public String getUemail() {
        return uemail;
    }

    public void setUemail(String uemail) {
        this.uemail = uemail;
    }

    public String getUstatus() {
        return ustatus;
    }

    public void setUstatus(String ustatus) {
        this.ustatus = ustatus;
    }

    public HRUser() {
    }

    public HRUser(int uid, String uname, String upwd, String usex, int uage, String uedu, int udeptid, HRDept hrDept, int upostid, HRPostition hrPostition, String umobile, String uintime, String uemail, String ustatus) {
        this.uid = uid;
        this.uname = uname;
        this.upwd = upwd;
        this.usex = usex;
        this.uage = uage;
        this.uedu = uedu;
        this.udeptid = udeptid;
        this.hrDept = hrDept;
        this.upostid = upostid;
        this.hrPostition = hrPostition;
        this.umobile = umobile;
        this.uintime = uintime;
        this.uemail = uemail;
        this.ustatus = ustatus;
    }

    @Override
    public String toString() {
        return "HRUser{" +
                "uid=" + uid +
                ", uname='" + uname + '\'' +
                ", upwd='" + upwd + '\'' +
                ", usex='" + usex + '\'' +
                ", uage=" + uage +
                ", uedu='" + uedu + '\'' +
                ", udeptid=" + udeptid +
                ", hrDept=" + hrDept +
                ", upostid=" + upostid +
                ", hrPostition=" + hrPostition +
                ", umobile='" + umobile + '\'' +
                ", uintime='" + uintime + '\'' +
                ", uemail='" + uemail + '\'' +
                ", ustatus='" + ustatus + '\'' +
                '}';
    }
}
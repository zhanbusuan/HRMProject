package com.hrm.entity;

import java.util.Date;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: 赖福灵
 * \* Date: 2020/4/3
 * \* Time: 16:33
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
public class HRVacation {
    private int vid;
    private int uid;
    private String vreason;
    private String vstime;
    private String vltime;
    private String vstatus;
    private String examReason;
    private int examUser;

    public int getVid() {
        return vid;
    }

    public void setVid(int vid) {
        this.vid = vid;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getVreason() {
        return vreason;
    }

    public void setVreason(String vreason) {
        this.vreason = vreason;
    }

    public String getVstime() {
        return vstime;
    }

    public void setVstime(String vstime) {
        this.vstime = vstime;
    }

    public String getVltime() {
        return vltime;
    }

    public void setVltime(String vltime) {
        this.vltime = vltime;
    }

    public String getVstatus() {
        return vstatus;
    }

    public void setVstatus(String vstatus) {
        this.vstatus = vstatus;
    }

    public String getExamReason() {
        return examReason;
    }

    public void setExamReason(String examReason) {
        this.examReason = examReason;
    }

    public int getExamUser() {
        return examUser;
    }

    public void setExamUser(int examUser) {
        this.examUser = examUser;
    }

    public HRVacation(int vid, int uid, String vreason, String vstime, String vltime, String vstatus, String examReason, int examUser) {
        this.vid = vid;
        this.uid = uid;
        this.vreason = vreason;
        this.vstime = vstime;
        this.vltime = vltime;
        this.vstatus = vstatus;
        this.examReason = examReason;
        this.examUser = examUser;
    }

    public HRVacation() {
    }

    @Override
    public String toString() {
        return "HRVacation{" +
                "vid=" + vid +
                ", uid=" + uid +
                ", vreason='" + vreason + '\'' +
                ", vstime='" + vstime + '\'' +
                ", vltime='" + vltime + '\'' +
                ", vstatus='" + vstatus + '\'' +
                ", examReason='" + examReason + '\'' +
                ", examUser=" + examUser +
                '}';
    }
}


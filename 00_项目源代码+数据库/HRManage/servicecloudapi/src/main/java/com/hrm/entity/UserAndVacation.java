package com.hrm.entity;

import java.util.Date;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: 赖福灵
 * \* Date: 2020/4/13
 * \* Time: 7:46
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
public class UserAndVacation {
    private int uid;
    private String uname;
    private String usex;
    private int uage;
    private String uedu;
    private int udeptid;
    private int upostid;
    private String umobile;
    private String uintime;
    private String uemail;
    private int vid;
    private String vreason;
    private String vstime;
    private String vltime;
    private String vstatus;
    private String examReason;
    private int examUser;

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getUsex() {
        return usex;
    }

    public void setUsex(String usex) {
        this.usex = usex;
    }

    public int getUage() {
        return uage;
    }

    public void setUage(int uage) {
        this.uage = uage;
    }

    public String getUedu() {
        return uedu;
    }

    public void setUedu(String uedu) {
        this.uedu = uedu;
    }

    public int getUdeptid() {
        return udeptid;
    }

    public void setUdeptid(int udeptid) {
        this.udeptid = udeptid;
    }

    public int getUpostid() {
        return upostid;
    }

    public void setUpostid(int upostid) {
        this.upostid = upostid;
    }

    public String getUmobile() {
        return umobile;
    }

    public void setUmobile(String umobile) {
        this.umobile = umobile;
    }

    public String getUintime() {
        return uintime;
    }

    public void setUintime(String uintime) {
        this.uintime = uintime;
    }

    public String getUemail() {
        return uemail;
    }

    public void setUemail(String uemail) {
        this.uemail = uemail;
    }

    public int getVid() {
        return vid;
    }

    public void setVid(int vid) {
        this.vid = vid;
    }

    public String getVreason() {
        return vreason;
    }

    public void setVreason(String vreason) {
        this.vreason = vreason;
    }

    public String getVstime() {
        return vstime;
    }

    public void setVstime(String vstime) {
        this.vstime = vstime;
    }

    public String getVltime() {
        return vltime;
    }

    public void setVltime(String vltime) {
        this.vltime = vltime;
    }

    public String getVstatus() {
        return vstatus;
    }

    public void setVstatus(String vstatus) {
        this.vstatus = vstatus;
    }

    public String getExamReason() {
        return examReason;
    }

    public void setExamReason(String examReason) {
        this.examReason = examReason;
    }

    public int getExamUser() {
        return examUser;
    }

    public void setExamUser(int examUser) {
        this.examUser = examUser;
    }

    public UserAndVacation(int uid, String uname, String usex, int uage, String uedu, int udeptid, int upostid, String umobile, String uintime, String uemail, int vid, String vreason, String vstime, String vltime, String vstatus, String examReason, int examUser) {
        this.uid = uid;
        this.uname = uname;
        this.usex = usex;
        this.uage = uage;
        this.uedu = uedu;
        this.udeptid = udeptid;
        this.upostid = upostid;
        this.umobile = umobile;
        this.uintime = uintime;
        this.uemail = uemail;
        this.vid = vid;
        this.vreason = vreason;
        this.vstime = vstime;
        this.vltime = vltime;
        this.vstatus = vstatus;
        this.examReason = examReason;
        this.examUser = examUser;
    }

    public UserAndVacation() {
    }

    @Override
    public String toString() {
        return "UserAndVacation{" +
                "uid=" + uid +
                ", uname='" + uname + '\'' +
                ", usex='" + usex + '\'' +
                ", uage=" + uage +
                ", uedu='" + uedu + '\'' +
                ", udeptid=" + udeptid +
                ", upostid=" + upostid +
                ", umobile='" + umobile + '\'' +
                ", uintime='" + uintime + '\'' +
                ", uemail='" + uemail + '\'' +
                ", vid=" + vid +
                ", vreason='" + vreason + '\'' +
                ", vstime='" + vstime + '\'' +
                ", vltime='" + vltime + '\'' +
                ", vstatus='" + vstatus + '\'' +
                ", examReason='" + examReason + '\'' +
                ", examUser=" + examUser +
                '}';
    }
}


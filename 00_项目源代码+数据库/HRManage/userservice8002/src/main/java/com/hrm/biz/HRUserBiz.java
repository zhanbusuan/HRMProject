package com.hrm.biz;

import com.github.pagehelper.PageInfo;
import com.hrm.entity.HRUser;

import java.util.List;

public interface HRUserBiz {
    public List<HRUser> queryAllHRUser();
    public boolean updateHRUser(HRUser hrUser);
    public boolean insertHRUser(HRUser hrUser);
    public boolean deleteHRUserByUid(int uid);
    public PageInfo<HRUser> queryByUnameOrDeptOrStatus(HRUser hrUser,int pagenum);
    public List<HRUser> queryByUname(String  uname);
//    public int queryByQueryHRUserCount(QueryHRUser queryHRUser);
    public HRUser queryUserByUid(int uid);
    public String login(String uid,String upwd);
    public boolean upadtePwd(HRUser hrUser);
    public void addBatch(List<HRUser> addBatch);
}

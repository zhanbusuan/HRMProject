package com.hrm.biz.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hrm.biz.HRUserBiz;
import com.hrm.dao.HRUserDao;
import com.hrm.entity.HRUser;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: WYJ
 * \* Date: 2020/4/3
 * \* Time: 11:50
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@Service
public class HRUserBizImpl implements HRUserBiz {
    @Resource
    private HRUserDao hrUserDao;

    @Override
    public List<HRUser> queryAllHRUser() {
        return hrUserDao.queryAllHRUser();
    }

    @Override
    public boolean updateHRUser(HRUser hrUser) {
        return hrUserDao.updateHRUser(hrUser);
    }

    @Override
    public boolean insertHRUser(HRUser hrUser) {
        return hrUserDao.insertHRUser(hrUser);
    }

    @Override
    public boolean deleteHRUserByUid(int uid) {
        return hrUserDao.deleteHRUserByUid(uid);
    }

    @Override
    public PageInfo<HRUser> queryByUnameOrDeptOrStatus(HRUser hrUser, int pagenum) {
        PageHelper.startPage(pagenum, 3);
        List<HRUser> list = hrUserDao.queryByUnameOrDeptOrStatus(hrUser);
        PageInfo<HRUser> page = new PageInfo<>(list);
        return page;
    }

    @Override
    public List<HRUser> queryByUname(String uname) {
        return hrUserDao.queryByUname(uname);
    }

//    @Override
//    public List<HRUser> queryByQueryUser(QueryHRUser queryHRUser) {
//        int size=queryHRUser.getRows();
//        int page=queryHRUser.getPage();
//        int count=hrUserDao.queryByQueryHRUserCount(queryHRUser);
//        count=count%size==0?count/size:(count/size)+1;
//        if (count==0){
//            return null;
//        }
//        page=page<1?1:page;
//        page=page>count?count:page;
//        queryHRUser.setPage(page);
//        queryHRUser.setStart((page-1)*size);
//        queryHRUser.setPagecount(count);
//        queryHRUser.setNext(page+1);
//        queryHRUser.setLast(page-1);
//        return hrUserDao.queryByQueryUser(queryHRUser);
//    }
//
//    @Override
//    public int queryByQueryHRUserCount(QueryHRUser queryHRUser) {
//        return hrUserDao.queryByQueryHRUserCount(queryHRUser);
//    }

    @Override
    public HRUser queryUserByUid(int uid) {
        return hrUserDao.queryUserByUid(uid);
    }

    @Override
    public String login(String uid, String upwd) {
        if(uid.equals("admin")){
            if (upwd.equals("123")){
                return "admin";
            }
            return "管理员密码错误";
        } else {
            int uid_int = Integer.parseInt(uid);
            HRUser hrUser = hrUserDao.queryUserByUid(uid_int);
            if(hrUser==null){
                return "用户名不存在";
            }else {
                if (hrUser.getUpwd().equals(upwd)){
                    return "登录成功";
                }
                return "用户名或密码错误";
            }
        }
    }

    @Override
    public boolean upadtePwd(HRUser hrUser) {
        return hrUserDao.upadtePwd(hrUser);
    }

    @Override
    public void addBatch(List<HRUser> addBatch) {
        hrUserDao.insertBatch(addBatch);
    }
}
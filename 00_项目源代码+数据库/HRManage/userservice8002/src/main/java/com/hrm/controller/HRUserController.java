package com.hrm.controller;

import com.github.pagehelper.PageInfo;
import com.hrm.biz.HRUserBiz;
import com.hrm.entity.HRUser;
import com.hrm.util.ExcelBeanUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: WYJ
 * \* Date: 2020/4/3
 * \* Time: 11:53
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@Controller
public class HRUserController {
    @Resource
    private HRUserBiz hrUserBiz;

    @GetMapping("queryAllHRUser")
    @ResponseBody
    public List<HRUser> queryAllHRUser(){

        return hrUserBiz.queryAllHRUser();
    }
    @PostMapping("updateHRUser")
    @ResponseBody
    public boolean updateHRUser(@RequestBody HRUser hrUser){
        System.out.println(hrUser);
        System.out.println(hrUserBiz.updateHRUser(hrUser));
        return hrUserBiz.updateHRUser(hrUser);
    }
    @PostMapping("insertHRUser")
    @ResponseBody
    public boolean insertHRUser(@RequestBody HRUser hrUser){
        return hrUserBiz.insertHRUser(hrUser);
    }
    @PostMapping("deleteHRUserByUid")
    @ResponseBody
    public boolean deleteHRUserByUid(@RequestParam int uid){
        return hrUserBiz.deleteHRUserByUid(uid);
    }
    @RequestMapping("queryUser")
    public String queryUser(Model model){
        List<HRUser> hrUserList=hrUserBiz.queryAllHRUser();
        System.out.println(hrUserList);
        model.addAttribute("hrUserList",hrUserList);
        return "queryUser";
    }
    @PostMapping("queryByUnameOrDeptOrStatus")
    @ResponseBody
    public PageInfo<HRUser> queryByUnameOrDeptOrStatus(@RequestBody HRUser hrUser, @RequestParam int pagenum){
        return hrUserBiz.queryByUnameOrDeptOrStatus(hrUser,pagenum);
    }
//    @PostMapping("queryByQueryUser")
//    @ResponseBody
//    public List<HRUser> queryByQueryUser(@RequestBody QueryHRUser queryHRUser){
//        return hrUserBiz.queryByQueryUser(queryHRUser);
//    }
    @PostMapping("queryUserByUid")
    @ResponseBody
    public HRUser queryUserByUid(@RequestParam int uid){
        return hrUserBiz.queryUserByUid(uid);
    }


    @RequestMapping("queryByUname")
    @ResponseBody
    public List<HRUser> queryByUname(@RequestParam String uname){

        return hrUserBiz.queryByUname(uname);
    }

    @RequestMapping("login")
    @ResponseBody
    public String login(@RequestParam String uid,@RequestParam String upwd){
        return hrUserBiz.login(uid,upwd);
    }
    @RequestMapping("updatePwd")
    @ResponseBody
    public boolean updatePwd(@RequestBody HRUser hrUser){
        return hrUserBiz.upadtePwd(hrUser);
    }

    @RequestMapping("/admin_uploadExcel")
    @ResponseBody
    public void addBatch(@RequestBody List<HRUser> addBatch) {
        hrUserBiz.addBatch(addBatch);
    }
}
package com.hrm.dao;

import com.hrm.entity.HRUser;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface HRUserDao {
    public List<HRUser> queryAllHRUser();
    public boolean updateHRUser(HRUser hrUser);
    public boolean insertHRUser(HRUser hrUser);
    public boolean deleteHRUserByUid(int uid);
    public List<HRUser> queryByUnameOrDeptOrStatus(HRUser hrUser);
   public List<HRUser> queryByUname(String uname);
//    public int queryByQueryHRUserCount(QueryHRUser queryHRUser);
    public HRUser queryUserByUid(int uid);
    public boolean upadtePwd(HRUser hrUser);
    public void insertBatch(List<HRUser> insertBatch);
}

package com.hrm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class Vacation8001Application {

    public static void main(String[] args) {
        SpringApplication.run(Vacation8001Application.class, args);
    }

}

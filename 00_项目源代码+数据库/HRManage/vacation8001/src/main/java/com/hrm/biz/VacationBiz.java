package com.hrm.biz;

import com.hrm.entity.HRVacation;
import com.hrm.entity.UserAndVacation;

import java.util.Date;
import java.util.List;

public interface VacationBiz {
    public List<HRVacation> queryVacation();
    public List<UserAndVacation> queryVacationLevelTwo(int udeptid);
    public List<UserAndVacation> queryVacationLevelThree(int udeptid);
    public List<UserAndVacation> queryVacationLevelTwo_wait(int udeptid);
    public HRVacation queryVacationByVid(int vid);

    public List<HRVacation> queryVacationByUid(int uid);
    public List<HRVacation> passVacationByVstatus_passByUid(int uid);
    public List<HRVacation> backVacationByVstatus_backByUid(int uid);
    public List<HRVacation> waitVacationByVstatus_waitByUid(int uid);

    public List<HRVacation> queryVacationByExamuser(int examuser);
    public List<HRVacation> queryVacationByVstatus(String vstatus);
    public int vacationCount();
    public int vacationCountTwo(int udeptid);
    public int vacationCountThree(int udeptid);

    public int vacationCountByUid(int uid);
    public int vacationCountByVstatus_passByUid(int uid);
    public int vacationCountByVstatus_backByUid(int uid);
    public int vacationCountByVstatus_waitByUid(int uid);

    public int vacationCountByVstatus_pass();
    public int vacationCountByVstatus_back();
    public int vacationCountByVstatus_wait();

    public int vacationCountTwoByVstatus_wait(int udeptid);

    public List<HRVacation> mohuQueryVacation(int uid, int examuser, String vstatus, String vstime, String vslime);
    public boolean modVacation(HRVacation vacation);
    public boolean addVacation(HRVacation vacation);
    public boolean removeVacation(int vid);
}

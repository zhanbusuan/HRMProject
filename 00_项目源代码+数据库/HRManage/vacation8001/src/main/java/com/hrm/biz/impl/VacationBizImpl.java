package com.hrm.biz.impl;

import com.hrm.biz.VacationBiz;
import com.hrm.dao.VacationDao;
import com.hrm.entity.HRVacation;
import com.hrm.entity.UserAndVacation;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: 赖福灵
 * \* Date: 2020/4/3
 * \* Time: 11:55
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@Service
public class VacationBizImpl implements VacationBiz {
    @Resource
    private VacationDao dao;

    @Override
    public List<HRVacation> queryVacation() {
        return dao.queryVacation();
    }

    @Override
    public List<UserAndVacation> queryVacationLevelTwo(int udeptid) {
        return dao.queryVacationLevelTwo(udeptid);
    }

    @Override
    public List<UserAndVacation> queryVacationLevelThree(int udeptid) {
        return dao.queryVacationLevelThree(udeptid);
    }

    @Override
    public List<UserAndVacation> queryVacationLevelTwo_wait(int udeptid) {
        return dao.queryVacationLevelTwo_wait(udeptid);
    }

    @Override
    public HRVacation queryVacationByVid(int vid) {
        return dao.queryVacationByVid(vid);
    }

    @Override
    public List<HRVacation> queryVacationByUid(int uid) {
        return dao.queryVacationByUid(uid);
    }

    @Override
    public List<HRVacation> passVacationByVstatus_passByUid(int uid) {
        return dao.passVacationByVstatus_passByUid(uid);
    }

    @Override
    public List<HRVacation> backVacationByVstatus_backByUid(int uid) {
        return dao.backVacationByVstatus_backByUid(uid);
    }

    @Override
    public List<HRVacation> waitVacationByVstatus_waitByUid(int uid) {
        return dao.waitVacationByVstatus_waitByUid(uid);
    }

    @Override
    public List<HRVacation> queryVacationByExamuser(int examuser) {
        return dao.queryVacationByExamuser(examuser);
    }

    @Override
    public List<HRVacation> queryVacationByVstatus(String vstatus) {
        return dao.queryVacationByVstatus(vstatus);
    }

    @Override
    public int vacationCount() {
        return dao.vacationCount();
    }

    @Override
    public int vacationCountTwo(int udeptid) {
        return dao.vacationCountTwo(udeptid);
    }

    @Override
    public int vacationCountThree(int udeptid) {
        return dao.vacationCountThree(udeptid);
    }

    @Override
    public int vacationCountByUid(int uid) {
        return dao.vacationCountByUid(uid);
    }

    @Override
    public int vacationCountByVstatus_passByUid(int uid) {
        return dao.vacationCountByVstatus_passByUid(uid);
    }

    @Override
    public int vacationCountByVstatus_backByUid(int uid) {
        return dao.vacationCountByVstatus_backByUid(uid);
    }

    @Override
    public int vacationCountByVstatus_waitByUid(int uid) {
        return dao.vacationCountByVstatus_waitByUid(uid);
    }

    @Override
    public int vacationCountByVstatus_pass() {
        return dao.vacationCountByVstatus_pass();
    }

    @Override
    public int vacationCountByVstatus_back() {
        return dao.vacationCountByVstatus_back();
    }

    @Override
    public int vacationCountByVstatus_wait() {
        return dao.vacationCountByVstatus_wait();
    }

    @Override
    public int vacationCountTwoByVstatus_wait(int udeptid) {
        return dao.vacationCountTwoByVstatus_wait(udeptid);
    }

    @Override
    public List<HRVacation> mohuQueryVacation(int uid, int examuser, String vstatus, String vstime, String vslime) {
        return dao.mohuQueryVacation(uid, examuser, vstatus, vstime, vslime);
    }

    @Override
    public boolean modVacation(HRVacation vacation) {
        return dao.modVacation(vacation);
    }

    @Override
    public boolean addVacation(HRVacation vacation) {
        return dao.addVacation(vacation);
    }

    @Override
    public boolean removeVacation(int vid) {
        return dao.removeVacation(vid);
    }

    public VacationDao getDao() {
        return dao;
    }

    public void setDao(VacationDao dao) {
        this.dao = dao;
    }
}


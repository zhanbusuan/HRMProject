package com.hrm.controller;

import com.hrm.biz.VacationBiz;
import com.hrm.entity.HRVacation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: 赖福灵
 * \* Date: 2020/4/8
 * \* Time: 11:19
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@Controller
@RequestMapping("/admin")
public class ManageVacationController {
    @Resource
    private VacationBiz vacationBiz;
    /*  通过申请  */
    @RequestMapping("/passApply")
    @ResponseBody
    public String passApply(int vid, int uid) {
        HRVacation vacation = vacationBiz.queryVacationByVid(vid);
        if("已驳回".equals(vacation.getVstatus())) {
            return "不能对已驳回的申请通过！";
        }
        vacation.setVstatus("已通过");
        vacation.setExamUser(uid);
        if(vacationBiz.modVacation(vacation)) {
            return "操作成功！";
        }else {
            return "操作失败！";
        }
    }
    /*  驳回申请  */
    @RequestMapping("/refuseApply")
    @ResponseBody
    public String backApply(int vid, int uid, String examReason) {
        HRVacation vacation = vacationBiz.queryVacationByVid(vid);
        if("已通过".equals(vacation.getVstatus())) {
            return "不能对已通过的申请驳回！";
        }
        vacation.setVstatus("已驳回");
        vacation.setExamReason(examReason);
        vacation.setExamUser(uid);
        if(vacationBiz.modVacation(vacation)) {
            return "操作成功！";
        }else {
            return "操作失败！";
        }
    }
}


package com.hrm.controller;

import com.hrm.biz.VacationBiz;
import com.hrm.entity.HRVacation;
import com.hrm.entity.UserAndVacation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: 赖福灵
 * \* Date: 2020/4/3
 * \* Time: 14:45
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@RestController
@RequestMapping("/vacation")
public class VacationController {
    @Resource
    private VacationBiz vacationBiz;

    /* 查询所有请假记录 */
    @RequestMapping("/queryVacation")
    public List<HRVacation> queryVacation() {
        return vacationBiz.queryVacation();
    }

    /* 根据vid查询请假记录 */
    @RequestMapping("/queryVacationByVid")
    public HRVacation queryVacationByVid(int vid) {
        return vacationBiz.queryVacationByVid(vid);
    }

    /* 根据uid查询请假记录--员工*/
    @RequestMapping("/queryVacationByUid")
    public List<HRVacation> queryVacationByUid(int uid) {
        return vacationBiz.queryVacationByUid(uid);
    }

    /* 根据uid查询请假记录--2级 */
    @RequestMapping("/queryVacationByTwo")
    public List<UserAndVacation> queryVacationTwo(int udeptid) {
        return vacationBiz.queryVacationLevelTwo(udeptid);
    }

    /* 根据uid查询请假记录--3级 */
    @RequestMapping("/queryVacationByThree")
    public List<UserAndVacation> queryVacationThree(int udeptid) {
        return vacationBiz.queryVacationLevelThree(udeptid);
    }

    /* 根据uid查询已通过审批的请假记录--员工*/
    @RequestMapping("/queryPassVacationByUid")
    public List<HRVacation> queryPassVacationByUid(int uid) {
        return vacationBiz.passVacationByVstatus_passByUid(uid);
    }
    /* 根据uid查询已驳回审批的请假记录--员工*/
    @RequestMapping("/queryBackVacationByUid")
    public List<HRVacation> queryBackVacationByUid(int uid) {
        return vacationBiz.backVacationByVstatus_backByUid(uid);
    }
    /* 根据uid查询待审批的请假记录--员工*/
    @RequestMapping("/queryWaitVacationByUid")
    public List<HRVacation> queryWaitVacationByUid(int uid) {
        return vacationBiz.waitVacationByVstatus_waitByUid(uid);
    }

    /* 根据审批人id查询请假记录 */
    @RequestMapping("/queryVacationByExamuser")
    public List<HRVacation> queryVacationByExamUser(int examUser) {
        return vacationBiz.queryVacationByExamuser(examUser);
    }

    /* 根据审批状态查询请假记录 */
    @RequestMapping("/queryVacationByVstatus")
    public List<HRVacation> queryVacationByVstatus(String vstatus) {
        return vacationBiz.queryVacationByVstatus(vstatus);
    }
    /* 查询记录总条数 */
    @RequestMapping("/vacationCount")
    public String vacationCount() {
        return String.valueOf(vacationBiz.vacationCount());
    }
    /* 查询记录总条数--2级 */
    @RequestMapping("/vacationCountTwo")
    public String vacationCountTwo(int udeptid) {
        return String.valueOf(vacationBiz.vacationCountTwo(udeptid));
    }
    /* 查询记录总条数--3级 */
    @RequestMapping("/vacationCountThree")
    public String vacationCountThree(int udeptid) {
        return String.valueOf(vacationBiz.vacationCountThree(udeptid));
    }
    /* 查询待审批记录总条数--2级 */
    @RequestMapping("/vacationCountTwoWait")
    public String vacationCountTwoWait(int udeptid) {
        return String.valueOf(vacationBiz.vacationCountTwoByVstatus_wait(udeptid));
    }

    /* 查询待审批记录--2级 */
    @RequestMapping("/vacationTwoWait")
    public List<UserAndVacation> vacationTwoWait(int udeptid) {
        return vacationBiz.queryVacationLevelTwo_wait(udeptid);
    }

    /* 根据uid查询请假记录条数--员工 */
    @RequestMapping("/vacationCountByUid")
    public String vacationCountByUid(int uid) {
        return String.valueOf(vacationBiz.vacationCountByUid(uid));
    }

    /* 根据uid查询已通过的请假记录的数量 */
    @RequestMapping("/passVacationNumByUid")
    public String passVacationNumByUid(int uid) {
        return String.valueOf(vacationBiz.vacationCountByVstatus_passByUid(uid));
    }

    /* 根据uid查询待审批的请假记录的数量 */
    @RequestMapping("/waitVacationNumByUid")
    public String waitVacationNumByUid(int uid) {
        return String.valueOf(vacationBiz.vacationCountByVstatus_waitByUid(uid));
    }

    /* 根据uid查询已驳回的请假记录的数量 */
    @RequestMapping("/backVacationNumByUid")
    public String backVacationNumByUid(int uid) {
        return String.valueOf(vacationBiz.vacationCountByVstatus_backByUid(uid));
    }

    /* 根据uid查询已通过的请假记录的数量 */
    @RequestMapping("/passVacationNum")
    public String passVacationNum() {
        return String.valueOf(vacationBiz.vacationCountByVstatus_pass());
    }

    /* 待审批的请假记录的数量 */
    @RequestMapping("/waitVacationNum")
    public String waitVacationNum() {
        return String.valueOf(vacationBiz.vacationCountByVstatus_wait());
    }

    /* 已驳回的请假记录的数量 */
    @RequestMapping("/backVacationNum")
    public String backVacationNum() {
        return String.valueOf(vacationBiz.vacationCountByVstatus_back());
    }

    /* 模糊查询请假记录 */
    @RequestMapping("/mohuQueryVacation")
    public List<HRVacation> mohuQueryVacation(int uid, int examuser, String vstatus, String vstime, String vslime) {
        return vacationBiz.mohuQueryVacation(uid, examuser, vstatus, vstime, vslime);
    }
    /* 修改请假记录 */
    @RequestMapping("/modVacation")
    public boolean modVacation(HRVacation vacation) {
        return vacationBiz.modVacation(vacation);
    }

    /* 添加请假记录--员工 */
    @RequestMapping("/addVacation")
    public String addVacation(HRVacation vacation) {
        if(vacationBiz.addVacation(vacation)) {
            return "申请成功！请等待审批！";
        }else {
            return "申请失败！请重试！";
        }
    }

    /* 删除请假记录 */
    @RequestMapping("/removeVacation")
    public boolean removeVacation(int vid) {
        return vacationBiz.removeVacation(vid);
    }
}


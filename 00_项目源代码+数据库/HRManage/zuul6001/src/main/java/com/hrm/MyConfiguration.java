package com.hrm;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import com.github.pagehelper.PageInterceptor;
import org.apache.catalina.Context;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.embedded.tomcat.TomcatContextCustomizer;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.io.File;
import java.util.Properties;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: zt
 * \* Date: 2020/3/23
 * \* Time: 20:02
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@Configuration
public class MyConfiguration {
    @Bean
    public ServletRegistrationBean DruidStatViewServlet(){
        ServletRegistrationBean srb = new ServletRegistrationBean(new StatViewServlet(),"/druid/*");

        //添加管理页面的初始化参数：initParams
        //配置白名单，即允许访问druid管理页面的ip
        srb.addInitParameter("allow", "127.0.0.1");
        //配置黑名单，即不允许访问druid管理页面的ip
        srb.addInitParameter("deny", "192.168.103.57");
        //配置登陆的用户名和密码
        srb.addInitParameter("loginUsername", "admin");
        srb.addInitParameter("loginPassword", "admin");
        //配置是否可以重置
        srb.addInitParameter("resetEnble", "false");

        return srb;

    }

    //webStatFilter  配置管理web请求的功能
    @Bean
    public FilterRegistrationBean druidStatFilter(){
        FilterRegistrationBean frb = new FilterRegistrationBean(new WebStatFilter());
        frb.addUrlPatterns("/*");
        frb.addInitParameter("exclusions", "*.js,*.gif,*.jpg,*.png,*.css,/druid/*");
        return frb;

    }
    @Bean
    @ConfigurationProperties(prefix="spring.datasource")
    public DataSource druidDataSource(){
        return new DruidDataSource();
    }
    @Bean
    public PageInterceptor pageHelper(){
        PageInterceptor pegeInterceptor = new PageInterceptor();
        Properties properties = new Properties();
        properties.setProperty("helperDialect", "mysql");  //数据库
        properties.setProperty("reasonable", "true");
        properties.setProperty("supportMethodsArguments", "true");
        properties.setProperty("autoRuntimeDialect", "true");
        pegeInterceptor.setProperties(properties);
        return pegeInterceptor;
    }
    @Bean
    public WebServerFactoryCustomizer<TomcatServletWebServerFactory> customizer() {
        return new WebServerFactoryCustomizer<TomcatServletWebServerFactory>() {
            @Override
            public void customize(TomcatServletWebServerFactory factory) {
                factory.addContextCustomizers(new TomcatContextCustomizer() {
                    @Override
                    public void customize(Context context) {
                        String relativePath = "zuul6001/src/main/webapp";  //注意这里改成项目名
                        File docBaseFile = new File(relativePath); // 如果路径不存在，则把这个路径加入进去
                        if (docBaseFile.exists()) {
                            context.setDocBase(docBaseFile.getAbsolutePath());
                        }
                    }
                });
            }
        };
    }
}
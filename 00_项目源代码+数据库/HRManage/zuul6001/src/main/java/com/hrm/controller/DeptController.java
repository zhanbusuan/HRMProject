package com.hrm.controller;

import com.github.pagehelper.PageInfo;
import com.hrm.entity.HRDept;
import com.hrm.entity.HRPostition;
import com.hrm.feigns.DeptFeign;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: lsy
 * \* Date: 2020/4/12 0012
 * \* Time: 17:03
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@Controller
@RequestMapping("depts")
public class DeptController {

    @Resource
    private DeptFeign deptFeigns;

    @RequestMapping("querydept")
    public String querydept(Model model, int pagenum){
        PageInfo<HRDept> hrDeptList = deptFeigns.queryDeptByPage(pagenum);
        int pageCount=hrDeptList.getPages();
        int currentpage=hrDeptList.getPageNum();
        int prepage=hrDeptList.getPrePage();
        int nextpage=hrDeptList.getNextPage();
        model.addAttribute("hrDeptList",hrDeptList.getList());
        model.addAttribute("currentpage",currentpage);
        model.addAttribute("prepage",prepage);
        model.addAttribute("nextpage",nextpage);
        model.addAttribute("pagecount",pageCount);
        return "queryDept";
    }

    @RequestMapping("toupdatedept")
    public String toupdatedept(int deptid, Model model , HttpSession session) {
        String admin = (String) session.getAttribute("admin");
        if (admin!=null){
            HRDept hrDept=deptFeigns.getbyid(deptid);
            model.addAttribute("hrDept",hrDept);
            return "updateDept";
        }
        model.addAttribute("msg","您不是管理员，无权修改部门信息");
        return "forward:/depts/querydept?pagenum=1";
    }

    @RequestMapping("updatedept")
    public String updateUser(HRDept hrDept, Model model) {
        if (deptFeigns.update(hrDept)){
            model.addAttribute("msg","修改成功");
            return "forward:/depts/querydept?pagenum=1";
        }
        model.addAttribute("msg","部门名已存在，修改失败");
        return "forward:/depts/toupdatedept?deptid="+hrDept.getDeptid();
    }

    @RequestMapping("deletedept")
    public String deletedept(int deptid,Model model,HttpSession session) {
        String admin = (String) session.getAttribute("admin");
        if(admin!=null){
            if (deptFeigns.delete(deptid)){
                model.addAttribute("msg","删除成功");
                return "forward:/depts/querydept?pagenum=1";
            }
            model.addAttribute("msg","删除失败");
            return "forward:/depts/querydept?pagenum=1";
        }
        model.addAttribute("msg","您不是管理员，无权删除部门信息");
        return "forward:/depts/querydept?pagenum=1";
    }

    @RequestMapping("toadddept")
    public String toadddept(Model model,HttpSession session) {
        String admin = (String) session.getAttribute("admin");
        if (admin!=null){
            return "addDept";
        }
        model.addAttribute("msg","您不是管理员，无权添加部门信息");
        return "forward:/depts/querydept?pagenum=1";
    }

    @RequestMapping("adddept")
    public String adddept(HRDept hrDept, Model model) {
        System.out.println("________________________________________________hrdept:"+hrDept);
        if (deptFeigns.add(hrDept)){
            model.addAttribute("msg","添加成功");
            return "forward:/depts/querydept?pagenum=1";
        }
        model.addAttribute("msg","部门名已存在，添加失败");
        return "forward:/depts/toadddept";
    }


    //以上是部门


    //以下是职务

    @RequestMapping("queryposition")
    public String queryposition(Model model, int pagenum){
        PageInfo<HRPostition> hrPostitionList = deptFeigns.queryPostitionByPage(pagenum);
        int pageCount=hrPostitionList.getPages();
        int currentpage=hrPostitionList.getPageNum();
        int prepage=hrPostitionList.getPrePage();
        int nextpage=hrPostitionList.getNextPage();
        model.addAttribute("hrPositionList",hrPostitionList.getList());
        model.addAttribute("currentpage",currentpage);
        model.addAttribute("prepage",prepage);
        model.addAttribute("nextpage",nextpage);
        model.addAttribute("pagecount",pageCount);
        return "queryPosition";
    }

    @RequestMapping("toupdateposition")
    public String toupdateposition(int pid, Model model , HttpSession session) {
        String admin = (String) session.getAttribute("admin");
        if (admin!=null){
            HRPostition hrPostition=deptFeigns.getPosibyid(pid);
            model.addAttribute("hrPostition",hrPostition);
            return "updatePosition";
        }
        model.addAttribute("msg","您不是管理员，无权修改职务信息");
        return "forward:/depts/queryposition?pagenum=1";
    }

    @RequestMapping("updateposition")
    public String updateposition(HRPostition hrPostition, Model model) {
        if (deptFeigns.updatePosi(hrPostition)){
            model.addAttribute("msg","修改成功");
            return "forward:/depts/queryposition?pagenum=1";
        }
        model.addAttribute("msg","职务名已存在，修改失败");
        return "forward:/depts/toupdateposition?pid="+hrPostition.getPid();
    }

    @RequestMapping("deleteposition")
    public String deleteposition(int pid,Model model,HttpSession session) {
        String admin = (String) session.getAttribute("admin");
        if(admin!=null){
            if (deptFeigns.deletePosi(pid)){
                model.addAttribute("msg","删除成功");
                return "forward:/depts/queryposition?pagenum=1";
            }
            model.addAttribute("msg","删除失败");
            return "forward:/depts/queryposition?pagenum=1";
        }
        model.addAttribute("msg","您不是管理员，无权删除职务信息");
        return "forward:/depts/querydept?pagenum=1";
    }

    @RequestMapping("toaddposition")
    public String toaddposition(Model model,HttpSession session) {
        String admin = (String) session.getAttribute("admin");
        if (admin!=null){
            return "addPosition";
        }
        model.addAttribute("msg","您不是管理员，无权添加部门信息");
        return "forward:/depts/queryposition?pagenum=1";
    }

    @RequestMapping("addposition")
    public String addposition(HRPostition hrPostition, Model model) {
        if (deptFeigns.addPosi(hrPostition)){
            model.addAttribute("msg","添加成功");
            return "forward:/depts/queryposition?pagenum=1";
        }
        model.addAttribute("msg","职务名已存在，添加失败");
        return "forward:/depts/toaddposition";
    }
}

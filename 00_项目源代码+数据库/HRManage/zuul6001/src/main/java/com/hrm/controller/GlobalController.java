package com.hrm.controller;

import com.hrm.entity.HRDept;
import com.hrm.entity.HRPostition;
import com.hrm.entity.HRUser;
import com.hrm.feigns.DeptFeign;
import com.hrm.feigns.PostitionFeign;
import com.hrm.feigns.UserFeign;
import com.hrm.feigns.VacationFeign;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: zt
 * \* Date: 2020/4/3
 * \* Time: 15:19
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@Controller
public class GlobalController {
    @Resource
    private VacationFeign vacationFeign;
    @Resource
    private UserFeign userFeign;
    @Resource
    private DeptFeign deptFeign;
    @Resource
    private PostitionFeign postitionFeign;

    @RequestMapping("/tologin")
    public String tologin(){
        return "login";
    }
    @RequestMapping("/toindex")
    public String toindex(HttpSession session,Model model){
        HRUser hrUser=(HRUser)session.getAttribute("user");
        System.out.println("hruser"+hrUser);
        HRUser hrUser1=userFeign.queryUserByUid(hrUser.getUid());
        System.out.println("hruser1"+hrUser1);
        HRDept dept=deptFeign.getbyid(hrUser1.getUdeptid());
        hrUser1.setHrDept(dept);
        HRPostition postition=deptFeign.getPosibyid(hrUser1.getUpostid());
        hrUser1.setHrPostition(postition);
        model.addAttribute("hruser",hrUser1);
        return "index";
    }
    @RequestMapping("/torecord")
    public String torecord(){
        return "record";
    }
    @RequestMapping("/toqueryUser")
    public String toqueryUser(){
        return "queryUser";
    }
    @RequestMapping("/toaddVacation")
    public String toaddVacation(){
        return "addVacation";
    }

    /* 去员工请假记录界面 */
    @RequestMapping("/tomyVacation")
    public String tomyVacation(Model model, HttpSession session){
        HRUser user = (HRUser) session.getAttribute("user");
        if(user==null) {
            model.addAttribute("msg", "请先登陆！");
            return "index";
        }
        session.setAttribute("myVacationList", vacationFeign.queryVacationByUid(user.getUid()));//个人请假记录
        if(user.getUpostid()==2) {
            return "myVacationTwo";
        }if(user.getUpostid()==3) {
            return "myVacationThree";
        }
        return "myVacation";
    }
    /* 查看和管理所有请假记录---2级 */
    @RequestMapping("/tosecondManageVacation")
    public String tosecondManageVacation(Model model, HttpSession session){
        HRUser user = (HRUser) session.getAttribute("user");
        if(user==null) {
            model.addAttribute("msg", "请先登陆！");
            return "index";
        }
        session.setAttribute("secondVacationList", vacationFeign.queryVacationTwo(user.getUdeptid()));//查询所有2级职工请假记录并放到session
        System.out.println(session.getAttribute("secondVacationList"));
        return "secondManageVacation";
    }
    /* 查看和管理所有请假记录---3级 */
    @RequestMapping("/tothirdManageVacation")
    public String tothirdManageVacation(Model model, HttpSession session){
        HRUser user = (HRUser) session.getAttribute("user");
        if(user==null) {
            model.addAttribute("msg", "请先登陆！");
            return "index";
        }
        session.setAttribute("thirdVacationList", vacationFeign.queryVacationThree(user.getUdeptid()));//查询所有3级职工请假记录并放到session
        return "thirdManageVacation";
    }

    /* 查看待审批请假记录---2级 */
    @RequestMapping("/tosecondManageVacationWait")
    public String tosecondManageVacationWait(Model model, HttpSession session){
        HRUser user = (HRUser) session.getAttribute("user");
        if(user==null) {
            model.addAttribute("msg", "请先登陆！");
            return "index";
        }
        session.setAttribute("secondVacationListWait", vacationFeign.vacationTwoWait(user.getUdeptid()));//查询2级职工请假的待审批记录并放到session
        return "secondManageVacationWait";
    }

    @RequestMapping("/testmyVacation")
    public String testmyVacation(){
        return "myVacation";
    }

    @RequestMapping("toquerydept")
    public String toquerydept(){
        return "queryDept";
    }

    @RequestMapping("toqueryposition")
    public String toqueryposition(){
        return "queryPosition";
    }
    @RequestMapping("touploadExcel")
    public String touploadExcel(){
        return "userUploadExcel";
    }
}
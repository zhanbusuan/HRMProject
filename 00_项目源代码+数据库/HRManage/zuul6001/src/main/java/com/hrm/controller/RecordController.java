package com.hrm.controller;

import com.github.pagehelper.PageInfo;
import com.hrm.entity.HRDept;
import com.hrm.entity.HRPostition;
import com.hrm.entity.HRRecord;
import com.hrm.entity.HRUser;
import com.hrm.feigns.DeptFeign;
import com.hrm.feigns.RecordFeign;
import com.hrm.feigns.UserFeign;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.List;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: zt
 * \* Date: 2020/4/5
 * \* Time: 19:35
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@Controller
@RequestMapping("record")
public class RecordController {
    @Resource
    private RecordFeign recordFeign;
    @Resource
    private DeptFeign deptFeign;

    @Resource
    private UserFeign userFeigns;

    @RequestMapping("add")
    public String add(HRRecord record,String uname,Model model){
        System.out.println("6001:   recordcontroller      add============接受到值=====");
        System.out.println(record);
        List<HRUser> userList= userFeigns.queryByUname(uname);
        if(userList!=null&&!userList.isEmpty()){

            for(int i=0;i<userList.size();i++){
                HRUser user =userList.get(i);
                if(user.getUdeptid()==record.getOdeptid()){
                    if(user.getUpostid()==record.getOpoisid()){
                        record.setUid(user.getUid());
                        //  record.setUname(uname);
                        //  record.setUser(user);
                    } else {
                        System.out.println("找不到user");
                        model.addAttribute("msg","所选职位没有该用户，请重新输入");
                        return "forward:/record/toadd";
                    }
                }else {
                    System.out.println("找不到user");
                    model.addAttribute("msg","所选部门没有该用户，请重新输入");
                    return "forward:/record/toadd";
                }
            }
            System.out.println(record);
           /* if(""==record.getChangetime()&&record.getChangetime()==null){
                System.out.println("时间为空");
                model.addAttribute("msg","调换时间不能为空");
                return "forward:/record/toadd";
            }else {*/

                recordFeign.add(record);
                HRUser u =userFeigns.queryUserByUid(record.getUid());
                u.setUdeptid(record.getNdeptid());
                u.setUpostid(record.getNpoisid());
                userFeigns.updateHRUser(u);
                return "forward:/record/list?pagenum=1";
//            }
        }else {
            model.addAttribute("msg","该用户不存在，请重新输入");
            return "forward:/record/toadd";
        }
    }

    @RequestMapping("toadd")
    public String toadd(Model model){
        System.out.println("进来添加=======");
        List<HRDept> dlist = deptFeign.getdeptList();
        List<HRPostition> plist = deptFeign.getpositionList();
        model.addAttribute("plist",plist);
        model.addAttribute("dlist",dlist);
        return "addrecord";
    }


    @RequestMapping("list")
    public String list(int pagenum,Model model){
        PageInfo<HRRecord> page=recordFeign.list(pagenum);
        List<HRRecord> rlist = page.getList();
        System.out.println(rlist);
        int pageCount = page.getPages();
        int currentpage = page.getPageNum();
        int prepage = page.getPrePage();
        int nextpage = page.getNextPage();
        List<HRDept> dlist = deptFeign.getdeptList();
        List<HRPostition> plist = deptFeign.getpositionList();
        model.addAttribute("plist",plist);
        model.addAttribute("dlist",dlist);
        model.addAttribute("pageCount",pageCount);
        model.addAttribute("currentpage",currentpage);
        model.addAttribute("prepage",prepage);
        model.addAttribute("nextpage",nextpage);
        model.addAttribute("rlist",rlist);
        return "record";
    }


    @RequestMapping("queryByParam")
    public String queryByParam(int pagenum,HRRecord record,Model model){
        PageInfo<HRRecord> page=recordFeign.listByParam(pagenum,record);
        List<HRRecord> rlist = page.getList();
        System.out.println(rlist);
        int pageCount = page.getPages();
        int currentpage = page.getPageNum();
        int prepage = page.getPrePage();
        int nextpage = page.getNextPage();
        List<HRDept> dlist = deptFeign.getdeptList();
        List<HRPostition> plist = deptFeign.getpositionList();
        model.addAttribute("record",record);
        model.addAttribute("plist",plist);
        model.addAttribute("dlist",dlist);
        model.addAttribute("pageCount",pageCount);
        model.addAttribute("currentpage",currentpage);
        model.addAttribute("prepage",prepage);
        model.addAttribute("nextpage",nextpage);
        model.addAttribute("rlist",rlist);
        return "record";
    }


    @RequestMapping("toupdate")
    public String toupdate(int  rid,Model model){
        HRRecord record =recordFeign.getOne(rid);
        List<HRDept> dlist = deptFeign.getdeptList();
        List<HRPostition> plist = deptFeign.getpositionList();
        model.addAttribute("plist",plist);
        model.addAttribute("dlist",dlist);
        model.addAttribute("record",record);
        return "updaterecord";
    }

    @RequestMapping("update")
    public String update(HRRecord record,Model model){
        System.out.println(record);
        if (recordFeign.updaterecord(record)){
            HRUser u =userFeigns.queryUserByUid(record.getUid());
            u.setUdeptid(record.getNdeptid());
            u.setUpostid(record.getNpoisid());
            userFeigns.updateHRUser(u);
            return "forward:/record/list?pagenum=1";
        }else {
            model.addAttribute("msg","更新失败，请重试");
            return "forward:/record/toupdate?rid="+record.getRid();
        }
    }
    @RequestMapping("delete")
    public String delete(int rid){
        recordFeign.deleterecord(rid);
        return "forward:/record/list?pagenum=1";
    }
}
package com.hrm.controller;

import com.github.pagehelper.PageInfo;
import com.hrm.entity.HRDept;
import com.hrm.entity.HRPostition;
import com.hrm.entity.HRUser;
import com.hrm.feigns.DeptFeign;
import com.hrm.feigns.PostitionFeign;
import com.hrm.feigns.UserFeign;
import com.hrm.util.ExcelBeanUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: WYJ
 * \* Date: 2020/4/3
 * \* Time: 16:56
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@Controller
@RequestMapping("users")
public class UserController {
    @Resource
    private UserFeign userFeigns;
    @Resource
    private DeptFeign deptFeigns;
   /* @Resource
    private PostitionFeign postFeigns;*/
    @RequestMapping("queryUser")
    public String queryUser(HRUser hrUser, Model model,int pagenum){
        List<HRDept> hrDeptList=deptFeigns.getdeptList();
        model.addAttribute("hrDeptList",hrDeptList);
        List<HRPostition> hrPostitionList= deptFeigns.getpositionList();
        model.addAttribute("hrPostitionList",hrPostitionList);
        System.out.println(hrUser);
        PageInfo<HRUser> hrUserList = userFeigns.queryByUnameOrDeptOrStatus(hrUser,pagenum);
        int pageCount=hrUserList.getPages();
        int currentpage=hrUserList.getPageNum();
        int prepage=hrUserList.getPrePage();
        int nextpage=hrUserList.getNextPage();
        for(HRUser user:hrUserList.getList()){
            HRDept hrDept=deptFeigns.getbyid(user.getUdeptid());
            user.setHrDept(hrDept);
            HRPostition hrPostition=deptFeigns.getPosibyid(user.getUpostid());
            user.setHrPostition(hrPostition);
        }
        model.addAttribute("hrUserList",hrUserList.getList());
        model.addAttribute("currentpage",currentpage);
        model.addAttribute("prepage",prepage);
        model.addAttribute("nextpage",nextpage);
        model.addAttribute("pagecount",pageCount);
        model.addAttribute("uname",hrUser.getUname());
        model.addAttribute("udeptid",hrUser.getUdeptid());
        model.addAttribute("ustatus",hrUser.getUstatus());
        return "queryAllUser";
    }
//    public void fenye(int page,List<HRUser> list,int size,Model model){
//        PageInfo pageInfo=new PageInfo(list);
//        int count=(int)pageInfo.getTotal();
//        int pageCount=count%size==0?(count/size):(count/size+1);
//        model.addAttribute("count",count);
//        model.addAttribute("pageCount",pageCount);
//        model.addAttribute("page",page);
//        model.addAttribute("hrUserList",list);
//    }
         //更新用户信息
         @RequestMapping("toUpdateUser")
        public String toUpdateUser(int uid, Model model) {
             List<HRDept> hrDeptList=deptFeigns.getdeptList();
             model.addAttribute("hrDeptList",hrDeptList);
             List<HRPostition> hrPostitionList= deptFeigns.getpositionList();
             model.addAttribute("hrPostitionList",hrPostitionList);
            HRUser hrUser=userFeigns.queryUserByUid(uid);
            HRDept hrDept=deptFeigns.getbyid(hrUser.getUdeptid());
            HRPostition hrPostition=deptFeigns.getPosibyid(hrUser.getUpostid());
            hrUser.setHrDept(hrDept);
            hrUser.setHrPostition(hrPostition);
            model.addAttribute("hrUser",hrUser);
            return "updateUser";
        }
       @RequestMapping("updateUser")
       public String updateUser(HRUser hrUser, Model model) {
           String mobile = "[1][3578]\\d{9}";
           String email = "([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)";
           if (hrUser.getUmobile().matches(mobile)==true) {
               if (hrUser.getUemail().matches(email)==true){
                   boolean flag=userFeigns.updateHRUser(hrUser);
                   model.addAttribute("msg", flag ? "修改成功" : "修改失败");
                   return "forward:/users/queryUser?pagenum=1";
               }else {
                   model.addAttribute("msg","电子邮箱输入格式不正确，请重新输入!");
                   return "forward:/users/toUpdateUser";
               }
           }else{
               model.addAttribute("msg","电话号码输入格式不正确，请重新输入!");
               return "forward:/users/toUpdateUser";
           }
    }
    //删除员工
    @RequestMapping("deleteUser")
    public String deleteUser(int uid,Model model) {
        boolean flag=userFeigns.deleteHRUserByUid(uid);
        model.addAttribute("msg", flag ? "删除成功" : "删除失败");
        return "forward:/users/queryUser?pagenum=1";
    }
    //添加员工
    @RequestMapping("toaddUser")
    public String toaddUser(HRUser hrUser,Model model) {
        List<HRDept> hrDeptList=deptFeigns.getdeptList();
        model.addAttribute("hrUser",hrUser);
        List<HRPostition> hrPostitionList= deptFeigns.getpositionList();
        model.addAttribute("hrPostitionList",hrPostitionList);
        model.addAttribute("hrDeptList",hrDeptList);
        return "addUser";
    }
    @RequestMapping("addUser")
    public String addUser(HRUser hrUser, Model model) {
        System.out.println(hrUser);
        if (hrUser.getUname()==null||hrUser.getUpwd()==null||hrUser.getUage()==0||hrUser.getUintime()==null){
            model.addAttribute("msg","用户信息输入不能为空!");
            return "forward:/users/toaddUser";
        }else {
            String mobile = "[1][3578]\\d{9}";
            String email = "([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)";
            if (hrUser.getUmobile().matches(mobile) == true) {
                if (hrUser.getUemail().matches(email) == true) {
                    boolean flag = userFeigns.insertHRUser(hrUser);
                    model.addAttribute("msg", flag ? "添加成功" : "添加失败");
                    return "forward:/users/queryUser?pagenum=1";
                } else {
                    model.addAttribute("msg", "电子邮箱输入格式不正确，请重新输入!");
                    return "forward:/users/toaddUser";
                }
            } else {
                model.addAttribute("msg", "电话号码输入格式不正确，请重新输入!");
                return "forward:/users/toaddUser";
            }
        }
    }

    @PostMapping("login")
    public String login(String uid_string, String upwd, Model model, HttpSession session){
        String returnstring = new String();
        if (uid_string.equals("用户名")){
            returnstring = "用户名不能为空";
            model.addAttribute("msg",returnstring);
            return "login";
        }
        if (upwd.isEmpty()){
            returnstring = "密码不能为空";
            model.addAttribute("msg",returnstring);
            return "login";
        }
        if (uid_string.equals("admin")){
            returnstring = userFeigns.login(uid_string,upwd);
            if (returnstring.equals("admin")){
                session.setAttribute("admin","admin");
                return "index";
            } else {
                model.addAttribute("msg",returnstring);
                return "login";
            }
        }
        if (!uid_string.matches("[0-9]+")){
            returnstring = "用户名应为纯数字";
            model.addAttribute("msg",returnstring);
            return "login";
        }
        returnstring = userFeigns.login(uid_string,upwd);
        model.addAttribute("msg",returnstring);
        if (returnstring.equals("登录成功")){
            int uid_int = Integer.parseInt(uid_string);
            HRUser user = userFeigns.queryUserByUid(uid_int);
            session.setAttribute("user",user);
            HRUser hrUser1=userFeigns.queryUserByUid(user.getUid());
            System.out.println("hruser1"+hrUser1);
            HRDept dept=deptFeigns.getbyid(hrUser1.getUdeptid());
            hrUser1.setHrDept(dept);
            HRPostition postition=deptFeigns.getPosibyid(hrUser1.getUpostid());
            hrUser1.setHrPostition(postition);
            model.addAttribute("hruser",hrUser1);
            return "index";
        }
        return "login";
    }
    //修改密码
    @RequestMapping("toupdatePwd")
    public String toupdatePwd( HttpSession session,Model model) {
        HRUser hrUser=(HRUser)session.getAttribute("user");
        HRUser hrUser1=userFeigns.queryUserByUid(hrUser.getUid());
        model.addAttribute("hruser",hrUser1);
        return "updatePwd";
    }
    @RequestMapping("updatePwd")
    public String updatePwd(String upwd,String pwd, String repwd, HttpSession session,Model model) {
        HRUser hrUser=(HRUser)session.getAttribute("user");
        HRUser hrUser1=userFeigns.queryUserByUid(hrUser.getUid());
            if (pwd.isEmpty() || repwd.isEmpty() || upwd.isEmpty()) {
                model.addAttribute("msg", "输入不能为空!");
                return "forward:/users/toupdatePwd";
            } else {
                if (upwd.equals(hrUser1.getUpwd())) {
                    if (!pwd.equals(repwd)) {
                        model.addAttribute("msg", "两次密码输入不相同!");
                        return "forward:/users/toupdatePwd";
                    } else {
                        hrUser1.setUpwd(pwd);
                        boolean flag = userFeigns.updatePwd(hrUser1);
                        model.addAttribute("hruser", hrUser1);
                        model.addAttribute("msg", flag ? "密码修改成功!" : "密码修改失败!");
                        return "forward:/toindex";
                    }
                }
                else{
                    model.addAttribute("msg","原密码输入错误");
                    return "forward:/users/toupdatePwd";
                }
            }

    }
    @RequestMapping("/admin_uploadExcel")
    public String adminUploadExcel(MultipartHttpServletRequest request, Map<String,Object> map, HttpSession session) throws IOException {
        MultipartFile file=request.getFile("file");
        List<HRUser> addBatch = new ArrayList<HRUser>();
        try {
            HSSFWorkbook wb=new HSSFWorkbook(file.getInputStream());
            addBatch= ExcelBeanUtil.readExcel(wb,map);     //返回excel中的学生集合
            userFeigns.addBatch(addBatch);
            return "forward:/users/queryUser?pagenum=1";
        }catch (Exception e) {
            e.printStackTrace();
            return "userUploadExcel";
        }
    }

}
package com.hrm.controller;

import com.hrm.entity.HRUser;
import com.hrm.entity.HRVacation;
import com.hrm.feigns.VacationFeign;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: 赖福灵
 * \* Date: 2020/4/6
 * \* Time: 10:13
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@Controller
public class VacationController {
    @Resource
    private VacationFeign vacationFeign;
    /*  查询所有请假记录  */
    @RequestMapping("/queryVacation")
    public List<HRVacation> queryVacation() {
        return vacationFeign.queryVacation();
    }

    /* 根据uid查询已通过审批的请假记录--员工*/
    @RequestMapping("/queryPassVacationByUid")
    public String queryPassVacationByUid(HttpSession session) {
        HRUser user = (HRUser) session.getAttribute("user");
        if(user==null) {
            session.setAttribute("msg", "请先登陆！");
            return "login";
        }
        List<HRVacation> myPassList = vacationFeign.queryPassVacationByUid(user.getUid());
        session.setAttribute("myPassList", myPassList);
        return "myPassVacation";
    }
    /* 根据uid查询已驳回审批的请假记录--员工*/
    @RequestMapping("/queryBackVacationByUid")
    public String queryBackVacationByUid(HttpSession session) {
        HRUser user = (HRUser) session.getAttribute("user");
        if(user==null) {
            session.setAttribute("msg", "请先登陆！");
            return "login";
        }
        List<HRVacation> myBackList = vacationFeign.queryBackVacationByUid(user.getUid());
        session.setAttribute("myBackList", myBackList);
        return "myBackVacation";
    }
    /* 根据uid查询待审批的请假记录--员工*/
    @RequestMapping("/queryWaitVacationByUid")
    public String queryWaitVacationByUid(HttpSession session) {
        HRUser user = (HRUser) session.getAttribute("user");
        if(user==null) {
            session.setAttribute("msg", "请先登陆！");
            return "login";
        }
        List<HRVacation> myWaitList = vacationFeign.queryWaitVacationByUid(user.getUid());
        session.setAttribute("myWaitList", myWaitList);
        return "myWaitVacation";
    }
}


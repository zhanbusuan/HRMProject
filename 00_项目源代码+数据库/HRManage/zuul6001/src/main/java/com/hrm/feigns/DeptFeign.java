package com.hrm.feigns;

import com.github.pagehelper.PageInfo;
import com.hrm.entity.HRDept;
import com.hrm.entity.HRPostition;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name="deptservice")
public interface DeptFeign {
    @RequestMapping("dept/getbyid")
    public HRDept getbyid(@RequestParam int id);
    @RequestMapping("dept/add")
    public boolean add(@RequestBody HRDept hrDept);
    @RequestMapping("dept/update")
    public boolean update(@RequestBody HRDept hrDept);
    @RequestMapping("dept/delete")
    public boolean delete(@RequestParam int id);
    @GetMapping("dept/getdeptlist")
    public List<HRDept> getdeptList();
    @RequestMapping("dept/querydeptbypage")
    public PageInfo<HRDept> queryDeptByPage(@RequestParam int pagenum);
    @RequestMapping("position/getPosibyid")
    public HRPostition getPosibyid(@RequestParam int id);
    @RequestMapping("position/add")
    public boolean addPosi(@RequestBody HRPostition hrPostition);
    @RequestMapping("position/update")
    public boolean updatePosi(@RequestBody HRPostition hrPostition);
    @RequestMapping("position/delete")
    public boolean deletePosi(@RequestParam int id);
    @GetMapping("position/getpositionlist")
    public List<HRPostition> getpositionList();
    @RequestMapping("position/querypostitionbypage")
    public PageInfo<HRPostition> queryPostitionByPage(@RequestParam int pagenum);
}

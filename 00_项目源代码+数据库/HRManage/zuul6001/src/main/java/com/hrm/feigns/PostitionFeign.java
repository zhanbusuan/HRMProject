package com.hrm.feigns;

import com.hrm.entity.HRPostition;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "positionservice")
public interface PostitionFeign {
    @RequestMapping("position/getbyid")
    public HRPostition getbyid(@RequestParam int id);
    @RequestMapping("position/add")
    public boolean add(HRPostition hrPostition);
    @RequestMapping("position/update")
    public boolean update(HRPostition hrPostition);
    @RequestMapping("position/delete")
    public boolean delete(int id);
    @GetMapping("position/getpositionlist")
    public List<HRPostition> getpositionList();
}

package com.hrm.feigns;

import com.github.pagehelper.PageInfo;
import com.hrm.entity.HRRecord;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: zt
 * \* Date: 2020/4/3
 * \* Time: 20:10
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@FeignClient(name="recordservice")
public interface RecordFeign {
    @RequestMapping("myrecord/list")
    public PageInfo<HRRecord> list(@RequestParam int pagenum);


    @RequestMapping("myrecord/listByParam")
    public PageInfo<HRRecord> listByParam(@RequestParam int pagenum, @RequestBody HRRecord record);

    @RequestMapping("myrecord/add")
    public boolean add(@RequestBody HRRecord record);

    @RequestMapping("/myrecord/delete")
    public boolean deleterecord(@RequestParam  int rid);

    @RequestMapping("/myrecord/update")
    public boolean updaterecord(@RequestBody HRRecord record);
    @RequestMapping("myrecord/getOne")
    public HRRecord getOne(@RequestParam int rid );
}
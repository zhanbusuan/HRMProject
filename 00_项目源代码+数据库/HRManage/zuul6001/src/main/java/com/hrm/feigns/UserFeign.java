package com.hrm.feigns;

import com.github.pagehelper.PageInfo;
import com.hrm.entity.HRUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: WYJ
 * \* Date: 2020/4/3
 * \* Time: 16:58
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@FeignClient(name = "userservice")
public interface UserFeign {
    @GetMapping("queryAllHRUser")
    public List<HRUser> queryAllHRUser();
    @PostMapping("updateHRUser")
    public boolean updateHRUser(@RequestBody HRUser hrUser);
    @PostMapping("insertHRUser")
    public boolean insertHRUser(@RequestBody HRUser hrUser);
    @PostMapping("deleteHRUserByUid")
    public boolean deleteHRUserByUid(@RequestParam int uid);
    @PostMapping("queryByUnameOrDeptOrStatus")
//    public List<HRUser> queryByUnameOrDeptOrStatus(@RequestBody HRUser hrUser);
    public PageInfo<HRUser> queryByUnameOrDeptOrStatus(@RequestBody HRUser hrUser, @RequestParam int pagenum);
    @PostMapping("queryUserByUid")
    public HRUser queryUserByUid(@RequestParam int uid);

    @RequestMapping("queryByUname")
    @ResponseBody
    public List<HRUser> queryByUname(@RequestParam String uname);
//    @PostMapping("queryByQueryUser")
//    public List<HRUser> queryByQueryUser(@RequestBody QueryHRUser queryHRUser);

    @RequestMapping("login")
    @ResponseBody
    public String login(@RequestParam String uid,@RequestParam String upwd);

    @RequestMapping("updatePwd")
    @ResponseBody
    public boolean updatePwd(@RequestBody HRUser hrUser);
    @RequestMapping("/admin_uploadExcel")
    @ResponseBody
    public void addBatch(@RequestBody List<HRUser> addBatch);
}
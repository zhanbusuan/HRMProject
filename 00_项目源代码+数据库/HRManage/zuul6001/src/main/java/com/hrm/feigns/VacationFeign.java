package com.hrm.feigns;

import com.hrm.entity.HRVacation;
import com.hrm.entity.UserAndVacation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "vacation8001")
public interface VacationFeign {
    /*  查询所有请假记录  */
    @RequestMapping("/vacation/queryVacation")
    public List<HRVacation> queryVacation();
    /*  查询个人所有请假记录  */
    @RequestMapping("/vacation/queryVacationByUid")
    public List<HRVacation> queryVacationByUid(@RequestParam int uid);
    /* 添加请假记录--员工 */
    @RequestMapping("/vacation/addVacation")
    public String addVacation(HRVacation vacation);

    /* 根据uid查询已通过审批的请假记录--员工*/
    @RequestMapping("/vacation/queryPassVacationByUid")
    public List<HRVacation> queryPassVacationByUid(@RequestParam int uid);
    /* 根据uid查询已驳回审批的请假记录--员工*/
    @RequestMapping("/vacation/queryBackVacationByUid")
    public List<HRVacation> queryBackVacationByUid(@RequestParam int uid);
    /* 根据uid查询待审批的请假记录--员工*/
    @RequestMapping("/vacation/queryWaitVacationByUid")
    public List<HRVacation> queryWaitVacationByUid(@RequestParam int uid);
    /* 根据uid查询请假记录--2级 */
    @RequestMapping("/vacation/queryVacationByTwo")
    public List<UserAndVacation> queryVacationTwo(@RequestParam int udeptid);
    /* 根据uid查询请假记录--3级 */
    @RequestMapping("/vacation/queryVacationByThree")
    public List<UserAndVacation> queryVacationThree(@RequestParam int udeptid);
    /* 查询待审批记录--2级 */
    @RequestMapping("/vacation/vacationTwoWait")
    public List<UserAndVacation> vacationTwoWait(@RequestParam int udeptid);

    /**  管理请假记录操作  **/
    /* 已通过的请假记录的数量 */
    @RequestMapping("/passVacationNum")
    public int passVacationNum();
    /* 待审批的请假记录的数量 */
    @RequestMapping("/waitVacationNum")
    public int waitVacationNum();
    /* 已驳回的请假记录的数量 */
    @RequestMapping("/backVacationNum")
    public int backVacationNum();
}

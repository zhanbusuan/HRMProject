package com.hrm.util;

import com.hrm.entity.HRUser;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ExcelBeanUtil {

//	public static List<Map<Integer, Object>> manageUserList(final List<HRUser> select_list){
//		List<Map<Integer, Object>> dataList=new ArrayList<>();
//		if (select_list!=null && select_list.size()>0) {
//			int length=select_list.size();
//			Map<Integer, Object> dataMap;
//			HRUser bean;
//			int index=0;
//			for (int i = 0; i <length; i++) {
//				bean=select_list.get(i);
//				index=i+1;
//				dataMap=new HashMap<>();
//				dataMap.put(0, index);	//第一列加入自增的序列号
////				dataMap.put(1, bean.getUid());
//				dataMap.put(1, bean.getUname());
//				dataMap.put(2, bean.getUpwd());
//				dataMap.put(3, bean.getTelephone());
//				dataMap.put(4, bean.getRole());
//				dataMap.put(5, bean.getEmail());
//				dataList.add(dataMap);
//			}
//		}
//		return dataList;
//	}

	public static List<HRUser> readExcel(HSSFWorkbook wb, Map<String,Object> map) {
		List<HRUser> list = new ArrayList<HRUser>();
		try {
			Row row = null;
			int numSheet = wb.getNumberOfSheets();
			System.out.println(numSheet+"bbbbb");
			if (numSheet > 0) {
				for (int i = 0; i < numSheet; i++) {
					Sheet sheet = wb.getSheetAt(i);
					int numRow = sheet.getLastRowNum();
					System.out.println(numRow+"cccccc");
					if (numRow > 0) {
						//获取表头信息，存入Map中（第一到第五行）
//						Row row3 = sheet.getRow(2);
//						String line3_c1 = ExcelUtil.manageCell(row3.getCell(0), null);
//						String line3_c2 = ExcelUtil.manageCell(row3.getCell(1), null);//因为这里是合并的单元格，所以从第五列开始取值
//						String cno=line3_c1.substring(line3_c1.indexOf("：")+1);	//课程号
//						String cname=line3_c2.substring(line3_c2.indexOf("：")+1);		//课程名
//						map.put("cno",cno);		//存入map中，便于取出
//						map.put("cname",cname);
//						System.out.println("cno="+cno);
//						System.out.println("cname="+cname);
//						Row row4 = sheet.getRow(3);
//						String line4_c1 = ExcelUtil.manageCell(row4.getCell(3), null);	//合并单元格
//						String tn=line4_c1.substring(line4_c1.indexOf("：")+1);	//教师姓名
//						map.put("tn",tn);
//						System.out.println("tn="+tn);


						for (int j = 1; j <= numRow; j++) {      //从第六行开始获取学生信息
							//TODO：跳过excel sheet表格头部，从第六行开始读取学生信息
							row = sheet.getRow(j);
							System.out.println(row+"dddddd");
							HRUser user = new HRUser();
//							String id = ExcelUtil.manageCell(row.getCell(1), null);   //获取第二列的数据
////							Integer uid = Integer.parseInt(id);
//							if (id.length() == 0)  //如果有空行，则跳过
//								continue;
							String uname = ExcelUtil.manageCell(row.getCell(1), null);
							System.out.println(uname);
							String upwd = ExcelUtil.manageCell(row.getCell(2), null);
							String usex = ExcelUtil.manageCell(row.getCell(3), null);
							String age = ExcelUtil.manageCell(row.getCell(4), null);
							Integer uage = Integer.parseInt(age);
							String uedu = ExcelUtil.manageCell(row.getCell(5), null);
							String deptid = ExcelUtil.manageCell(row.getCell(6), null);
							Integer udeptid = Integer.parseInt(deptid);
							String postid = ExcelUtil.manageCell(row.getCell(7), null);
							Integer upostid = Integer.parseInt(postid);
							String umobile = ExcelUtil.manageCell(row.getCell(8), null);
							String uintime = ExcelUtil.manageCell(row.getCell(9), null);
							String uemail = ExcelUtil.manageCell(row.getCell(10), null);
							String ustatus = ExcelUtil.manageCell(row.getCell(11), null);
							user.setUname(uname);
							user.setUpwd(upwd);
							user.setUsex(usex);
							user.setUage(uage);
							user.setUedu(uedu);
							user.setUdeptid(udeptid);
							user.setUpostid(upostid);
							user.setUmobile(umobile);
							user.setUintime(uintime);
							user.setUemail(uemail);
							user.setUstatus(ustatus);
							list.add(user);
						}
					}
				}
			}
		}catch (Exception e) {}
		return list;
	}

}













package com.hrm.util;

import org.apache.poi.ss.usermodel.Workbook;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.net.URLEncoder;

public class WebUtil {

	public static void downloadExcel(HttpServletResponse response,Workbook wb,String fileName) throws Exception{
		fileName = URLEncoder.encode(fileName, "UTF-8");
		fileName = new String(fileName.getBytes("utf-8"),"ISO-8859-1");		//将文件名进行转码
		response.setHeader("Content-Disposition", "attachment;filename="+new String(fileName.getBytes(),"iso-8859-1"));
		response.setContentType("application/ynd.ms-excel;charset=UTF-8");
		OutputStream out=response.getOutputStream();
		wb.write(out);
		out.flush();
		out.close();
	}
	
}































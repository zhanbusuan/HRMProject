<%--
  Created by IntelliJ IDEA.
  User: zt
  Date: 2020/4/3
  Time: 15:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>My JSP 'MyJsp.jsp' starting page</title>

    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
    <!--
    <link rel="stylesheet" type="text/css" href="styles.css">
    -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="人力资源管理平台" />
    <title>人力资源管理平台</title>
    <link type="text/css" rel="stylesheet" href="css/css.css" />
    <script type="text/javascript">
        var msg='${msg}';
        if(msg!=''){
            alert(msg);
        }


    </script>
</head>

<body>
<div class="header">
    <div class="top"> <img class="logo" src="images/logo.jpg" />
        <ul class="nav">
            <li class="seleli"><a href="toindex">首页</a></li>
            <li><a href="tomyVacation">个人中心</a></li>
            <li><a href="users/toupdatePwd">修改密码</a></li>
            <li><a href="depts/querydept?pagenum=1">部门管理</a></li>
            <li><a href="record/list?pagenum=1">人事管理</a></li>
            <li><a href="users/queryUser?pagenum=1">员工管理</a></li>
            <li><a href="positions/queryposition?pagenum=1">职务管理</a></li>
        </ul>
    </div>
</div>
<div class="container">
    <div class="leftbar">
        <div class="lm01"> <img class="peptx" src="images/tximg.jpg" />
            <div class="pepdet">
                <p class="pepname">李小雅</p>
                <p>李小雅</p>
                <p>江苏话务一部三组</p>
            </div>
            <div class="clear"></div>
        </div>
        <div class="lm02">
            <div class="title"><img class="icon" src="images/dataicon.jpg" />
                <h2>日历</h2>
            </div>
            <div class="detail"> <img class="" src="images/kj_01.jpg" /> </div>
        </div>
        <div class="lm03">
            <div class="title"><img style="padding-right:5px;" class="icon" src="images/weaicon.jpg" />
                <h2>天气</h2>
            </div>
            <div class="detail"> <img class="" src="images/kj_02.jpg" /> </div>
        </div>
    </div>
    <div class="mainbody">
        <div class="currmenu">
            <ul class="nav">
                <li class="seleli"><a href="toindex">首页</a><li>
                <c:if test="${admin eq null}">
                <li><a href="tomyVacation">请假申请</a><li>
                <li><a href="record/queryByParam?pagenum=1&uname=${sessionScope.user.uname}">我的职位调动</a><li>
                <li><a href="users/toupdatePwd">修改密码</a><li>
                </c:if>
                <c:if test="${admin !=null}">
                <li><a href="record/list?pagenum=1">人事管理</a><li>
                <li><a href="users/queryUser?pagenum=1">员工管理</a><li>
                <li><a href="depts/querydept?pagenum=1">部门管理</a><li>
                <li><a href="depts/queryposition?pagenum=1">职位管理</a><li>
                </c:if>

            </ul>
        </div>
        <div class="adtip">
            <div class="tip">
                <p class="goom">早上好，小雅！</p>
                <p>您目前有<span>15</span>条待办，<span>1</span>条考勤异常，<span>2</span>条通知！</p>
            </div>
            <div class="adv">
                <p>公司统一公告在这边展示</p>
                <span> x </span> </div>
        </div>
        <div class="rig_lm01">
            <div style="padding: 100px 100px 10px;">
                <form class="bs-example bs-example-form" role="form" action="users/addUser">
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon">姓名</span>
                        <input type="text" class="form-control" placeholder="请输入姓名" name="uname">
                    </div>
                    <br>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon">密码</span>
                        <input type="text" class="form-control" placeholder="请输入密码" name="upwd">
                    </div>
                    <br>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon">性别</span>
                        <select class="form-control" name="usex" >
                            <option value ="男">男</option>
                            <option value ="女">女</option>
                        </select>
                    </div>
                    <br>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon">年龄</span>
                        <input type="text" class="form-control" placeholder="请输入年龄" name="uage" value="0">
                    </div>
                    <br>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon">学历</span>
                        <select class="form-control" name="uedu">
                            <option value ="大专">大专</option>
                            <option value ="本科">本科</option>
                            <option value ="硕士">硕士</option>
                        </select>
                    </div>
                    <br>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon">部门</span>
                        <select class="form-control" name="udeptid" >
                            <c:forEach items="${hrDeptList}" var="h">
                                <option value="${h.deptid}">${h.dname}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <br>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon">职位</span>
                        <select class="form-control" name="upostid" >
                            <c:forEach items="${hrPostitionList}" var="h">
                                <option value="${h.pid}">${h.pname}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <br>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon">联系电话</span>
                        <input type="text" class="form-control" placeholder="请输入联系电话" name="umobile">
                    </div>
                    <br>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon">入职时间</span>
                        <input type="text" class="form-control" placeholder="请输入入职时间(YYYY-MM-DD)" name="uintime">
                    </div>
                    <br>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon">电子邮箱</span>
                        <input type="text" class="form-control" placeholder="请输入电子邮箱" name="uemail">
                    </div>
                    <br>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon">状态</span>
                        <select class="form-control" name="ustatus">
                            <option value ="在职">在职</option>
                            <option value ="离职">离职</option>
                            <option value ="请假">请假</option>
                        </select>
                    </div>
                    <button type="submit" value="添加">添加</button>
                </form>
            </div>
</div>
<div class="footer"></div>
    </div>
</div>
</body>
</html>

<%--
  Created by IntelliJ IDEA.
  User: zt
  Date: 2020/4/3
  Time: 15:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>My JSP 'MyJsp.jsp' starting page</title>

    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
    <!--
    <link rel="stylesheet" type="text/css" href="styles.css">
    -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="人力资源管理平台" />
    <title>人力资源管理平台</title>
    <link type="text/css" rel="stylesheet" href="css/css.css" />
    <script type="text/javascript">
        var msg='${msg}';
        if(msg!=''){
            alert(msg);
        }


    </script>
</head>

<body>
<div class="header">
    <div class="top"> <img class="logo" src="images/logo.jpg" />
        <ul class="nav">
            <li class="seleli"><a href="toindex">首页</a><li>
            <c:if test="${admin eq null}">
            <li><a href="tomyVacation">请假申请</a><li>
            <li><a href="record/queryByParam?pagenum=1&uname=${sessionScope.user.uname}">我的职位调动</a><li>
            <li><a href="users/toupdatePwd">修改密码</a><li>
            </c:if>
            <c:if test="${admin !=null}">
            <li><a href="record/list?pagenum=1">人事管理</a><li>
            <li><a href="users/queryUser?pagenum=1">员工管理</a><li>
            <li><a href="depts/querydept?pagenum=1">部门管理</a><li>
            <li><a href="depts/queryposition?pagenum=1">职位管理</a><li>
            </c:if>

        </ul>
    </div>
</div>
<div class="container">
    <div class="leftbar">
        <div class="lm01"> <img class="peptx" src="images/tximg.jpg" />
            <div class="pepdet">
                <p class="pepname">李小雅</p>
                <p>李小雅</p>
                <p>江苏话务一部三组</p>
            </div>
            <div class="clear"></div>
        </div>
        <div class="lm02">
            <div class="title"><img class="icon" src="images/dataicon.jpg" />
                <h2>日历</h2>
            </div>
            <div class="detail"> <img class="" src="images/kj_01.jpg" /> </div>
        </div>
        <div class="lm03">
            <div class="title"><img style="padding-right:5px;" class="icon" src="images/weaicon.jpg" />
                <h2>天气</h2>
            </div>
            <div class="detail"> <img class="" src="images/kj_02.jpg" /> </div>
        </div>
    </div>
    <div class="mainbody">
        <div class="currmenu">
            <ul class="rig_nav">
                <li class="rig_seleli"><a href="#">当前</a><span> x </span></li>
                <li><a href="#">个人中心</a></li>
                <li><a href="#">绩效管理</a></li>
                <li><a href="#">绩效计划制定</a></li>
            </ul>
        </div>
        <div class="adtip">
            <div class="tip">
                <p class="goom">早上好，小雅！</p>
                <p>您目前有<span>15</span>条待办，<span>1</span>条考勤异常，<span>2</span>条通知！
                    <input type="button" name="" onclick="toadd()"/><a href="record/add">添加</a>
                </p>
            </div>
        </div>
        <div class="rig_lm01">
            <form action="record/add" method="post">
            <table class="tabindex" width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td>员工姓名
                    </td>
                    <td>
                        <input type="text" name="uname">
                    </td>
                </tr>
                <tr>
                    <td>原部门
                    </td>
                    <td>

                       <%-- <select class="form-control" id="edit_subject"
                                name="subject">
                            <option value="">
                                --请选择--
                            </option>
                            <c:forEach items="${qsubjectList}" var="item">
                                <option value="${item.sname}"<c:if test="${item.sname==qrf.subject}"> selected</c:if>>
                                        ${item.sname }
                                </option>
                            </c:forEach>
                        </select>--%>
                        <select name="odeptid">
                            <c:forEach items="${dlist}" var="d">
                                <option value="${d.deptid}">${d.dname}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>原职位
                    </td>
                    <td>
                        <select name="opoisid">
                            <c:forEach items="${plist}" var="p">
                                <option value="${p.pid}">${p.pname}</option>
                            </c:forEach>

                        </select>
                    </td>
                </tr>
                <tr>
                    <td>新部门
                    </td>
                    <td>
                        <select name="ndeptid">
                        <c:forEach items="${dlist}" var="d">
                            <option value="${d.deptid}">${d.dname}</option>
                        </c:forEach>

                    </select>
                    </td>
                </tr>
                <tr>
                    <td>新职位
                    </td>
                    <td>
                        <select name="npoisid">
                        <c:forEach items="${plist}" var="p">
                            <option value="${p.pid}">${p.pname}</option>
                        </c:forEach>
                       </select>
                    </td>
                </tr>
                <tr>
                    <td>调动时间
                    </td>
                    <td>
                        <input type="text" name="changetime">
                    </td>
                </tr>
                <tr>
                    <td><button type="reset">重置</button>
                    </td>
                    <td>
                        <button type="submit">添加</button>
                    </td>
                </tr>
            </table>
            </form>
        </div>

    </div>
</div>
<div class="footer"></div>
</body>
</html>

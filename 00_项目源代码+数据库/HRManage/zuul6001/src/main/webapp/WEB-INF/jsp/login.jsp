<%--
  Created by IntelliJ IDEA.
  User: lsy
  Date: 2020/4/9 0009
  Time: 9:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta http-equiv="Expires" content="0">
    <title>后台管理</title>
    <link href="../css/login.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/sweetalert.min.js"></script>
    <script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript">
        var msg='${msg}';
        if(msg !=''){
            alert(msg);
        }
    </script>
</head>

<body>
    <div class="login_box">
        <div class="login_l_img"><img src="../images/login-img.png" style="width: auto;" /></div>
        <div class="login">
            <div class="login_logo"><a href="#"><img src="../images/login_logo.png" /></a></div>
            <div class="login_name">
                <p>人力资源管理系统</p>
            </div>
            <form method="post" action="users/login">
                <input name="uid_string" type="text"  value="用户名" onfocus="this.value=''" onblur="if(this.value==''){this.value='用户名'}">
                <span id="password_text" onclick="this.style.display='none';document.getElementById('password').style.display='block';document.getElementById('password').focus().select();" >密码</span>
                <input name="upwd" type="password" id="password" style="display:none;" onblur="if(this.value==''){document.getElementById('password_text').style.display='block';this.style.display='none'};"/>
                <input value="登录" style="width:100%;" type="submit">
            </form>
        </div>
    </div>
</body>
</html>

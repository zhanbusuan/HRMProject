<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>我的请假</title>

    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
    <!--
    <link rel="stylesheet" type="text/css" href="styles.css">
    -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="人力资源管理平台" />
    <link type="text/css" rel="stylesheet" href="../css/css.css" />

    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        var uid = ${user.uid};
        $(function () {
           $.get("myvacation/vacation/vacationCountByUid", {uid:uid}, function (data) {
               $("#vacationCount").html(data);
           });
           $.get("myvacation/vacation/vacationCountByUid", {uid:uid}, function (data) {
                $("#vacationCountByUid").html(data);
            });
            $.get("myvacation/vacation/passVacationNumByUid", {uid:uid}, function (data) {
                $("#passVacationNum").html(data);
            });
            $.get("myvacation/vacation/waitVacationNumByUid", {uid:uid}, function (data) {
                $("#waitVacationNum").html(data);
            });
            $.get("myvacation/vacation/backVacationNumByUid", {uid:uid}, function (data) {
                $("#backVacationNum").html(data);
            });
        });
    </script>
</head>

<body>
<div class="header" style="height: 60px; margin-top: -10px;">
    <div class="top"> <img class="logo" src="../images/logo.jpg"/>
        <ul class="nav">
            <li class="seleli"><a href="toindex">首页</a><li>
            <c:if test="${admin eq null}">
            <li><a href="tomyVacation">请假申请</a><li>
            <li><a href="record/queryByParam?pagenum=1&uname=${sessionScope.user.uname}">我的职位调动</a><li>
            <li><a href="users/toupdatePwd">修改密码</a><li>
            </c:if>
            <c:if test="${admin !=null}">
            <li><a href="record/list?pagenum=1">人事管理</a><li>
            <li><a href="users/queryUser?pagenum=1">员工管理</a><li>
            <li><a href="depts/querydept?pagenum=1">部门管理</a><li>
            <li><a href="depts/queryposition?pagenum=1">职位管理</a><li>
            </c:if>

        </ul>
    </div>
</div>
<div class="container">
    <div class="leftbar" style="margin-left: 0">
        <div class="lm01"> <img class="peptx" src="../images/tximg.jpg" />
            <div class="pepdet">
                <p class="pepname">李小雅</p>
                <p>李小雅</p>
                <p>江苏话务一部三组</p>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="mainbody">
        <div class="currmenu">
            <ul class="rig_nav">
                <li><a href="tomyVacation">我的请假</a></li>
                <li><a href="tosecondManageVacation">请假管理</a></li>
            </ul>
        </div>
        <div class="rig_lm03">
            <div class="detail">
                <div class="inner03">
                    <div id="tabCot_product" class="zhutitab">
                        <div class="tabContainer">
                            <ul class="tabHead" id="tabCot_product-li-currentBtn-">
                                <li>
                                    <a href="" title="请假申请" rel="1" data-toggle="modal" data-target="#vacationApplyModal">请假申请</a>
                                    <span class="red_numb" id="vacationCountByUid"></span>
                                </li>
                                <li>
                                    <a href="queryPassVacationByUid" title="已通过" rel="2">已通过</a>
                                    <span class="red_numb" id="passVacationNum"></span>
                                </li>
                                <li >
                                    <a href="queryBackVacationByUid" title="已驳回" rel="3">已驳回</a>
                                    <span class="red_numb" id="backVacationNum"></span>
                                </li>
                                <li >
                                    <a href="queryWaitVacationByUid" title="待审批" rel="3">待审批</a>
                                    <span class="red_numb" id="waitVacationNum"></span>
                                </li>
                            </ul>
                            <div class="clear"></div>
                        </div>
                        <div id="tabCot_product_1" class="tabCot" >
                            <table class="tabindex" width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th bgcolor="#f8f8f8" scope="col">记录编号</th>
                                    <th bgcolor="#f8f8f8" scope="col">请假原因</th>
                                    <th bgcolor="#f8f8f8" scope="col">请假时间</th>
                                    <th bgcolor="#f8f8f8" scope="col">请假状态</th>
                                    <th bgcolor="#f8f8f8" scope="col">驳回理由</th>
                                    <th bgcolor="#f8f8f8" scope="col">审批人ID</th>
                                </tr>
                                <c:forEach items="${myVacationList}" var="mvlist">
                                    <tr>
                                        <td bgcolor="#FFFFFF">${mvlist.vid}</td>
                                        <td class="datacol" bgcolor="#FFFFFF">${mvlist.vreason}</td>
                                        <td bgcolor="#FFFFFF">${mvlist.vstime}至${mvlist.vltime}</td>
                                        <td class="waitcol" bgcolor="#FFFFFF">${mvlist.vstatus}</td>
                                        <td class="waitcol" bgcolor="#FFFFFF">${mvlist.examReason}</td>
                                        <td class="waitcol" bgcolor="#FFFFFF">${mvlist.examUser}</td>
                                    </tr>
                                </c:forEach>
                            </table>
                            <div class="fanye">
                                <p class="fytip">Showing 1 to 10 of 12 entries</p>
                                <div class="yem">
                                    <ul>
                                        <li><a href="#">First</a></li>
                                        <li><a href="#">&lt;</a></li>
                                        <li class="sellify"><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">&gt;</a></li>
                                        <li><a href="#">Last</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div id="tabCot_product_2" class="tabCot"  style="display: none;"> 2222222222 </div>
                        <div id="tabCot_product_3" class="tabCot"  style="display: none;"> 3333333333 </div>
                        <script language="JavaScript" type="text/javascript" src="../js/tab.js"></script>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 模态框（Modal） -->
    <div class="modal fade" id="vacationApplyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="myModalLabel">请假申请</h4>
                </div>
                <div class="modal-body">
                    <label style="margin-right: 15px; vertical-align: top;">请假原因：</label>
                    <textarea rows="3" cols="30" id="vreason" style="resize: none;"></textarea><br><hr>
                    <label style="margin-right: 15px">请假时间：</label>
                    <input type="date" value="2020-04-07" id="vstime" /><label>&emsp;至&emsp;</label><input type="date" value="2020-04-07" id="vltime" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" onclick="addVacation()">提交</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal -->
    </div>
</div>
<div class="footer"></div>
<script type="text/javascript">
    function addVacation() {
        var uid = ${user.uid};
        var vreason = $("#vreason").val();
        var vstime = $("#vstime").val();
        var vltime = $("#vltime").val();
        if(vreason=="") {
            alert("请输入申请理由！");
            return false;
        }
        if(vstime=="") {
            alert("请输入请假时间！");
            return false;
        }
        if(vltime=="") {
            alert("请输入结束时间！");
            return false;
        }
        $.get("myvacation/vacation/addVacation", {uid:uid, vreason:vreason, vstime:vstime, vltime:vltime}, function (data) {
            alert(data);
            location.reload();
        });
    }
</script>
</body>
</html>
<%--
  Created by IntelliJ IDEA.
  User: lsy
  Date: 2020/4/12 0012
  Time: 20:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>My JSP 'MyJsp.jsp' starting page</title>

    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
    <!--
    <link rel="stylesheet" type="text/css" href="styles.css">
    -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="人力资源管理平台" />
    <title>人力资源管理平台</title>
    <link type="text/css" rel="stylesheet" href="css/css.css" />
    <script type="text/javascript">
        var msg='${msg}';
        if(msg!=''){
            alert(msg);
        }
    </script>
</head>

<body>
<div class="header">
    <div class="top"> <img class="logo" src="images/logo.jpg" />
        <ul class="nav">
            <li class="seleli"><a href="toindex">首页</a><li>
            <c:if test="${admin eq null}">
            <li><a href="tomyVacation">请假申请</a><li>
            <li><a href="record/queryByParam?pagenum=1&uname=${sessionScope.user.uname}">我的职位调动</a><li>
            <li><a href="users/toupdatePwd">修改密码</a><li>
            </c:if>
            <c:if test="${admin !=null}">
            <li><a href="record/list?pagenum=1">人事管理</a><li>
            <li><a href="users/queryUser?pagenum=1">员工管理</a><li>
            <li><a href="depts/querydept?pagenum=1">部门管理</a><li>
            <li><a href="depts/queryposition?pagenum=1">职位管理</a><li>
            </c:if>

        </ul>
    </div>
</div>
<div class="container">
    <div class="leftbar">
        <div class="lm01"> <img class="peptx" src="images/tximg.jpg" />
            <div class="pepdet">
                <p class="pepname">李小雅</p>
                <p>李小雅</p>
                <p>江苏话务一部三组</p>
            </div>
            <div class="clear"></div>
        </div>
        <div class="lm02">
            <div class="title"><img class="icon" src="images/dataicon.jpg" />
                <h2>日历</h2>
            </div>
            <div class="detail"> <img class="" src="images/kj_01.jpg" /> </div>
        </div>
        <div class="lm03">
            <div class="title"><img style="padding-right:5px;" class="icon" src="images/weaicon.jpg" />
                <h2>天气</h2>
            </div>
            <div class="detail"> <img class="" src="images/kj_02.jpg" /> </div>
        </div>
    </div>
    <div class="mainbody">
        <div class="currmenu">
            <ul class="rig_nav">
                <li class="rig_seleli"><a href="#">当前</a><span> x </span></li>
                <li><a href="#">个人中心</a></li>
                <li><a href="#">绩效管理</a></li>
                <li><a href="#">绩效计划制定</a></li>
            </ul>
        </div>
        <div class="adtip">
            <div class="tip">
                <p class="goom">早上好，小雅！</p>
                <p>您目前有<span>15</span>条待办，<span>1</span>条考勤异常，<span>2</span>条通知！</p>
            </div>
            <div class="adv">
                <p>公司统一公告在这边展示</p>
                <span> x </span> </div>
        </div>
        <div class="rig_lm01">
                <a class="btn btn-danger btn-xs" href="depts/toadddept">添加部门</a>

                <table class="tabindex" width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <th width="7%" bgcolor="#f8f8f8" scope="col"><div align="center">编号</div></th>
                        <th width="7%" bgcolor="#f8f8f8" scope="col"><span class="titlab">部门名</span></th>
                        <th width="9%" bgcolor="#f8f8f8" scope="col">操作</th>
                    </tr>
                    <tr>
                        <c:forEach items="${hrDeptList}" var="h">
                    <tr>
                        <td>
                                ${h.deptid}
                        </td>
                        <td>
                                ${h.dname }
                        </td>
                        <td>
                            <a class="btn btn-danger btn-xs"
                               href="depts/toupdatedept?deptid=${h.deptid}">修改</a>
                            <a class="btn btn-danger btn-xs"
                               href="depts/deletedept?deptid=${h.deptid}">删除</a>
                        </td>
                    </tr>
                    </c:forEach>
                    </tr>
                </table>
                <tr align="center">
                    <td colspan="6" align="center">
                        <a href="depts/querydept?pagenum=1">首页</a>
                        <a href="depts/querydept?pagenum=${prepage}">上一页</a>&nbsp;
                        <a href="depts/querydept?pagenum=${nextpage}">下一页</a>&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="depts/querydept?pagenum=${pagecount}">尾页</a>
                    </td>
                </tr>
        </div>
    </div>
</div>
<div class="footer"></div>
</body>
</html>

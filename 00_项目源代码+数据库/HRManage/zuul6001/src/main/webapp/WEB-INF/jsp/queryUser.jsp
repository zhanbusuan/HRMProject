<%--
  Created by IntelliJ IDEA.
  User: zt
  Date: 2020/4/3
  Time: 15:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>My JSP 'MyJsp.jsp' starting page</title>

    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
    <!--
    <link rel="stylesheet" type="text/css" href="styles.css">
    -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="人力资源管理平台" />
    <title>人力资源管理平台</title>
    <link type="text/css" rel="stylesheet" href="css/css.css" />

</head>

<body>
<div class="header">
    <div class="top"> <img class="logo" src="images/logo.jpg" />
        <ul class="nav">
            <li class="seleli"><a href="toindex">首页</a><li>
            <c:if test="${admin eq null}">
            <li><a href="tomyVacation">请假申请</a><li>
            <li><a href="record/queryByParam?pagenum=1&uname=${sessionScope.user.uname}">我的职位调动</a><li>
            <li><a href="users/toupdatePwd">修改密码</a><li>
            </c:if>
            <c:if test="${admin !=null}">
            <li><a href="record/list?pagenum=1">人事管理</a><li>
            <li><a href="users/queryUser?pagenum=1">员工管理</a><li>
            <li><a href="depts/querydept?pagenum=1">部门管理</a><li>
            <li><a href="depts/queryposition?pagenum=1">职位管理</a><li>
            </c:if>

        </ul>
    </div>
</div>
<div class="container">
    <div class="leftbar">
        <div class="lm01"> <img class="peptx" src="images/tximg.jpg" />
            <div class="pepdet">
                <p class="pepname">李小雅</p>
                <p>李小雅</p>
                <p>江苏话务一部三组</p>
            </div>
            <div class="clear"></div>
        </div>
        <div class="lm02">
            <div class="title"><img class="icon" src="images/dataicon.jpg" />
                <h2>日历</h2>
            </div>
            <div class="detail"> <img class="" src="images/kj_01.jpg" /> </div>
        </div>
        <div class="lm03">
            <div class="title"><img style="padding-right:5px;" class="icon" src="images/weaicon.jpg" />
                <h2>天气</h2>
            </div>
            <div class="detail"> <img class="" src="images/kj_02.jpg" /> </div>
        </div>
    </div>
    <div class="mainbody">
        <div class="currmenu">
            <ul class="rig_nav">
                <li class="rig_seleli"><a href="#">当前</a><span> x </span></li>
                <li><a href="#">个人中心</a></li>
                <li><a href="#">绩效管理</a></li>
                <li><a href="#">绩效计划制定</a></li>
            </ul>
        </div>
        <div class="adtip">
            <div class="tip">
                <p class="goom">早上好，小雅！</p>
                <p>您目前有<span>15</span>条待办，<span>1</span>条考勤异常，<span>2</span>条通知！</p>
            </div>
        </div>

    </div>
    <div class="rig_link">

        <div class="rig_lm03">
            <div class="title"><img src="images/listicon.jpg" class="icon" style="padding-top:13px;">
                <h2>待办事项</h2>
            </div>
            <div class="detail">
                <div class="inner03">
                    <div id="tabCot_product" class="zhutitab" style="width: 1000px">
                        <div class="tabContainer">
                            <ul class="tabHead" id="tabCot_product-li-currentBtn-">
                                <li class="currentBtn"><a href="javascript:void(0)" title="绩效考核" rel="1">绩效考核</a><span class="grey_numb">12</span></li>
                                <li ><a href="javascript:void(0)" title="人事考核" rel="2">人事考核</a><span class="red_numb">2</span></li>
                                <li ><a href="javascript:void(0)" title="TAB名称" rel="3">TAB名称</a><span class="red_numb">5</span></li>
                            </ul>
                            <p class="shent"><span>Show entries: </span>
                                <input style="width:30px;" type="text" value="10">
                                <img src="images/sz.jpg" class="icon" style=""></p>
                            <div class="clear"></div>
                        </div>
                        <div id="tabCot_product_1" class="tabCot" style="float: right">
                            <table class="tabindex" width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th width="7%" bgcolor="#f8f8f8" scope="col"><div align="center">编号</div></th>
                                    <th width="7%" bgcolor="#f8f8f8" scope="col"><span class="titlab">姓名</span><span class="xila">&or;</span></th>
                                    <th width="7%" bgcolor="#f8f8f8" scope="col"><span class="titlab">密码</span><span class="xila">&or;</span></th>
                                    <th width="7%" bgcolor="#f8f8f8" scope="col"><span class="titlab">性别</span><span class="xila">&or;</span></th>
                                    <th width="7%" bgcolor="#f8f8f8" scope="col"><span class="titlab">年龄</span><span class="xila">&or;</span></th>
                                    <th width="7%" bgcolor="#f8f8f8" scope="col"><span class="titlab">学历</span><span class="xila">&or;</span></th>
                                    <th width="7%" bgcolor="#f8f8f8" scope="col"><span class="titlab">部门</span><span class="xila">&or;</span></th>
                                    <th width="7%" bgcolor="#f8f8f8" scope="col"><span class="titlab">职位</span><span class="xila">&or;</span></th>
                                    <th width="9%" bgcolor="#f8f8f8" scope="col"><span class="titlab">联系电话</span><span class="xila">&or;</span></th>
                                    <th width="9%" bgcolor="#f8f8f8" scope="col"><span class="titlab">入职时间</span><span class="xila">&or;</span></th>
                                    <th width="9%" bgcolor="#f8f8f8" scope="col"><span class="titlab">电子邮箱</span><span class="xila">&or;</span></th>
                                    <th width="9%" bgcolor="#f8f8f8" scope="col"><span class="titlab">状态</span><span class="xila">&or;</span></th>
                                    <th width="9%" bgcolor="#f8f8f8" scope="col">操作</th>
                                </tr>
                                <tr>
                                    <c:forEach items="${hrUserList}" var="h">
                                        <td>
                                                ${h.uid}
                                        </td>
                                        <td>
                                                ${h.uname }
                                        </td>

                                        <td>
                                                ${h.upwd}
                                        </td>
                                        <td>
                                                ${h.usex }
                                        </td>
                                        <td>
                                                ${h.uage }
                                        </td>
                                        <td>
                                                ${h.uedu }
                                        </td>
                                        <td>
                                                ${h.udeptid }
                                        </td>
                                        <td>
                                                ${h.upostid }
                                        </td>
                                        <td>
                                                ${h.umobile }
                                        </td>
                                        <td>
                                                ${h.uintime }
                                        </td>
                                        <td>
                                                ${h.uemail }
                                        </td>
                                        <td>
                                                ${h.ustatus }
                                        </td>
                                        <td>
                                            <a class="btn btn-danger btn-xs"
                                               href="user/deleteuserorder.wyj?odid=${o.odid}">删除</a>
                                        </td>
                                    </c:forEach>
                                </tr>
                            </table>
                            <div class="fanye">
                                <p class="fytip">Showing 1 to 10 of 12 entries</p>
                                <div class="yem">
                                    <ul>
                                        <li><a href="#">First</a></li>
                                        <li><a href="#">&lt;</a></li>
                                        <li class="sellify"><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">&gt;</a></li>
                                        <li><a href="#">Last</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div id="tabCot_product_2" class="tabCot"  style="display: none;"> 2222222222 </div>
                        <div id="tabCot_product_3" class="tabCot"  style="display: none;"> 3333333333 </div>
                        <script language="JavaScript" type="text/javascript" src="js/tab.js"></script>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="footer"></div>
</body>
</html>

<%--
  Created by IntelliJ IDEA.
  User: zt
  Date: 2020/4/3
  Time: 15:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>My JSP 'MyJsp.jsp' starting page</title>

    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
    <!--
    <link rel="stylesheet" type="text/css" href="styles.css">
    -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="人力资源管理平台" />
    <title>人力资源管理平台</title>
    <link type="text/css" rel="stylesheet" href="css/css.css" />

</head>

<body>
<div class="header">
    <div class="top"> <img class="logo" src="images/logo.jpg" />
        <ul class="nav">
            <li class="seleli"><a href="toindex">首页</a><li>
            <c:if test="${admin eq null}">
            <li><a href="tomyVacation">请假申请</a><li>
            <li><a href="record/queryByParam?pagenum=1&uname=${sessionScope.user.uname}">我的职位调动</a><li>
            <li><a href="users/toupdatePwd">修改密码</a><li>
            </c:if>
            <c:if test="${admin !=null}">
            <li><a href="record/list?pagenum=1">人事管理</a><li>
            <li><a href="users/queryUser?pagenum=1">员工管理</a><li>
            <li><a href="depts/querydept?pagenum=1">部门管理</a><li>
            <li><a href="depts/queryposition?pagenum=1">职位管理</a><li>
            </c:if>

        </ul>
    </div>
</div>
<div class="container">
    <div class="leftbar">
        <div class="lm01"> <img class="peptx" src="images/tximg.jpg" />
            <div class="pepdet">
                <p class="pepname">李小雅</p>
                <p>李小雅</p>
                <p>江苏话务一部三组</p>
            </div>
            <div class="clear"></div>
        </div>
        <div class="lm02">
            <div class="title"><img class="icon" src="images/dataicon.jpg" />
                <h2>日历</h2>
            </div>
            <div class="detail"> <img class="" src="images/kj_01.jpg" /> </div>
        </div>
        <div class="lm03">
            <div class="title"><img style="padding-right:5px;" class="icon" src="images/weaicon.jpg" />
                <h2>天气</h2>
            </div>
            <div class="detail"> <img class="" src="images/kj_02.jpg" /> </div>
        </div>
    </div>
    <div class="mainbody">
        <div class="currmenu">
            <ul class="rig_nav">
                <li class="rig_seleli"><a href="#">当前</a><span> x </span></li>
                <li><a href="#">个人中心</a></li>
                <li><a href="#">绩效管理</a></li>
                <li><a href="#">绩效计划制定</a></li>
            </ul>
        </div>
        <div class="adtip">
            <div class="tip">
                <p class="goom">早上好，小雅！</p>
                <p>您目前有<span>15</span>条待办，<span>1</span>条考勤异常，<span>2</span>条通知！
                    <a href="record/toadd">添加</a>
                </p>
            </div>
        </div>
        <div class="tip">
            <form action="record/queryByParam?pagenum=1" method="post">
                <label>原部门</label>
                <select name="odeptid" id="odeptid">
                    <option value="0">
                        --请选择--
                    </option>
                    <c:forEach items="${dlist}" var="d">
                        <option value="${d.deptid}" <c:if test="${d.deptid==record.odeptid}"> selected</c:if>>${d.dname}</option>
                    </c:forEach>

                </select>
                <label>原职位</label>
                <select name="opoisid" id="opoisid">
                    <option value="0">
                        --请选择--
                    </option>
                    <c:forEach items="${plist}" var="p">
                        <option value="${p.pid}" <c:if test="${p.pid==record.opoisid}"> selected</c:if>>${p.pname}</option>
                    </c:forEach>

                </select>
                <label>新部门</label>
                <select name="ndeptid" id="ndeptid">
                    <option value="0">
                        --请选择--
                    </option>
                    <c:forEach items="${dlist}" var="d">
                        <option value="${d.deptid}" <c:if test="${d.deptid==record.ndeptid}"> selected</c:if>>${d.dname}</option>
                    </c:forEach>

                </select>
                <label>新职位</label>
                <select name="npoisid" id="npoisid">
                    <option value="0">
                        --请选择--
                    </option>
                    <c:forEach items="${plist}" var="p">
                        <option value="${p.pid}" <c:if test="${p.pid==record.npoisid}"> selected</c:if>>${p.pname}</option>
                    </c:forEach>
                </select>
                <label>调换时间</label>
                <span> <input type="text" id = "changetime" name="changetime" <c:if test="${record.changetime}">value="${record.changetime}" </c:if>><button type="submit">查询</button> </span>
            </form>
        </div>
        <div class="rig_lm01">
            <table class="tabindex" width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <%--  <th width="7%" bgcolor="#f8f8f8" scope="col"><div align="center">编号</div></th>--%>
                    <th width="7%" bgcolor="#f8f8f8" scope="col"><span class="titlab">姓名</span></th>
                    <th width="7%" bgcolor="#f8f8f8" scope="col"><span class="titlab">原部门</span></th>
                    <th width="7%" bgcolor="#f8f8f8" scope="col"><span class="titlab">原职位</span></th>
                    <th width="7%" bgcolor="#f8f8f8" scope="col"><span class="titlab">新部门</span></th>
                    <th width="7%" bgcolor="#f8f8f8" scope="col"><span class="titlab">新职位</span></th>
                    <th width="7%" bgcolor="#f8f8f8" scope="col"><span class="titlab">调换时间</span></th>
                    <th width="9%" bgcolor="#f8f8f8" scope="col">操作</th>
                </tr>
                    <c:forEach items="${rlist}" var="r">
                        <tr>
                            <td>
                               ${r.uname}
                            </td>
                            <td>
                                <c:forEach items="${dlist}" var="d">
                                    <c:if test="${r.odeptid==d.deptid}">${d.dname} </c:if>
                                </c:forEach>

                            </td>

                            <td>
                                <c:forEach items="${plist}" var="p">
                                    <c:if test="${r.opoisid==p.pid}">${p.pname} </c:if>
                                </c:forEach>

                            </td>
                            <td>
                                <c:forEach items="${dlist}" var="d">
                                    <c:if test="${r.ndeptid==d.deptid}">${d.dname} </c:if>
                                </c:forEach>

                            </td>
                            <td>
                                <c:forEach items="${plist}" var="p">
                                    <c:if test="${r.npoisid==p.pid}">${p.pname} </c:if>
                                </c:forEach>

                            </td>
                            <td>
                                    ${r.changetime}
                            </td>
                           <%-- <td>
                                ${r.odept.dname}
                            </td>

                            <td>
                                    ${r.opostition.pname}
                            </td>
                            <td>
                                    ${r.ndept.dname}
                            </td>
                            <td>
                                    ${r.npostition.pname}
                            </td>
                            <td>
                                    ${r.changetime}
                            </td>--%>

                            <td>

                                <a class="btn btn-primary btn-xs"
                                   href="record/toupdate?rid=${r.rid}">修改</a>
                                <a class="btn btn-danger btn-xs"
                                   href="record/delete?rid=${r.rid}">删除</a>
                                <!-- 模态框（Modal） -->


                            </td>
                         </tr>
                     </c:forEach>

            </table>
            <tr>
                <td colspan="6" align="center">

                    当前页数：${currentpage}&nbsp;&nbsp;&nbsp;&nbsp;
                    <a onclick="fenye(1)">首页</a>&nbsp;&nbsp;&nbsp;&nbsp;
                    <a onclick="fenye(${prepage})">上一页</a>&nbsp;&nbsp;&nbsp;&nbsp;
                    <a onclick="fenye(${nextpage})">下一页</a>&nbsp;&nbsp;&nbsp;&nbsp;
                    <a onclick="fenye(${pageCount})">尾页</a>&nbsp;&nbsp;&nbsp;&nbsp;
                    总页数：${pageCount }
                </td>
            </tr>
           <%-- <tr>
                当前页：${currentpage}
                <a href="record/list?pagenum=1">首页</a>
                <a href="record/list?pagenum=${prepage}">上一页</a>
                <a href="record/list?pagenum=${nextpage}">下一页</a>
                <a href="record/list?pagenum=${pageCount}">尾页</a>
                总页数：${pageCount}
            </tr>--%>
        </div>
    </div>
</div>
<div class="footer"></div>
<script type="text/javascript">


    function fenye(page) {
        var odeptid = document.getElementById("odeptid").value;
        var opoisid = document.getElementById("opoisid").value;
        var npoisid = document.getElementById("npoisid").value;
        var ndeptid = document.getElementById("ndeptid").value;
        var changetime = document.getElementById("changetime").value;
        var url = "record/queryByParam?pagenum="+page;
        if(odeptid!=0&&odeptid!=null){
            url+="&odeptid="+odeptid;
        }
        if(opoisid!=0&&opoisid!=null){
            url+="&opoisid="+opoisid;
        }
        if(ndeptid!=0&&ndeptid!=null){
            url+="&ndeptid="+ndeptid;
        }
        if(npoisid!=0&&npoisid!=null){
            url+="&npoisid="+npoisid;
        }
        if(changetime!=""&&changetime!=null){
            url+="&changetime="+changetime;
        }
        location=url;

    }
</script>
</body>
</html>

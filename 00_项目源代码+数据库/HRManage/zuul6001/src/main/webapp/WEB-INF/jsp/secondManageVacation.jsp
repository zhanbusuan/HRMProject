<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>员工请假管理</title>

    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="人力资源管理平台" />
    <link type="text/css" rel="stylesheet" href="../css/css.css" />

    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>

    <style type="text/css">
        #leftDiv{
            border: 1px solid blueviolet;
            margin-left: 1%;
            width: 18%;
            height: 80%;
            float: left;
        }
        #topDiv,#middleDiv,#bottomDiv{
            width: 80%;
            float: left;
        }
    </style>

    <script type="text/javascript">
        var udeptid = ${user.udeptid};
        $(function () {
            $.get("myvacation/vacation/vacationCountTwo", {udeptid:udeptid}, function (data) {
                $("#vacationCountTwo").html(data);
            });
            $.get("myvacation/vacation/vacationCountTwoWait", {udeptid:udeptid}, function (data) {
                $("#vacationCountTwoWait").html(data);
            });
            $.get("myvacation/vacation/passVacationNumByUid", {uid:uid}, function (data) {
                $("#passVacationNum").html(data);
            });
            $.get("myvacation/vacation/waitVacationNumByUid", {uid:uid}, function (data) {
                $("#waitVacationNum").html(data);
            });
            $.get("myvacation/vacation/backVacationNumByUid", {uid:uid}, function (data) {
                $("#backVacationNum").html(data);
            });
        });
        function passApply(vid, uid) {
            var msg = "确定通过申请？";
            if(confirm(msg)) {
                $.get("myvacation/admin/passApply", {vid:vid, uid:uid}, function (data) {
                    alert(data);
                    if(data=="操作成功！") {
                        location.reload();
                    }
                });
            }
        }
        function refuseApply(vid, uid, examReason) {
            if(examReason=="") {
                alert("请填写驳回理由！");
                return false;
            }
            var msg = "确定驳回申请？";
            if(confirm(msg)) {
                $.get("myvacation/admin/refuseApply", {vid:vid, uid:uid, examReason:examReason}, function (data) {
                    alert(data);
                    if(data=="操作成功！") {
                        location.reload();
                    }
                });
            }
        }
        function addVid(vid) {
            $("#vid").val(vid);
        }
    </script>
</head>

<body>
<div class="header" style="width: 98%; margin-left: 1%; margin-right: 1%; height: 60px; margin-top: -10px;">
    <div class="top"> <img class="logo" src="../images/logo.jpg" />
        <ul class="nav">
            <li class="seleli"><a href="toindex">首页</a><li>
            <c:if test="${admin eq null}">
            <li><a href="tomyVacation">请假申请</a><li>
            <li><a href="record/queryByParam?pagenum=1&uname=${sessionScope.user.uname}">我的职位调动</a><li>
            <li><a href="users/toupdatePwd">修改密码</a><li>
            </c:if>
            <c:if test="${admin !=null}">
            <li><a href="record/list?pagenum=1">人事管理</a><li>
            <li><a href="users/queryUser?pagenum=1">员工管理</a><li>
            <li><a href="depts/querydept?pagenum=1">部门管理</a><li>
            <li><a href="depts/queryposition?pagenum=1">职位管理</a><li>
            </c:if>

        </ul>
    </div>
</div>
<div class="container" style="width: 98%; margin-left: 1%; margin-right: 1%;">
    <div>
        <div id="leftDiv"></div>
        <div id="topDiv" class="currmenu">
            <ul class="rig_nav">
                <li><a href="tomyVacation">我的请假</a></li>
                <li><a href="tosecondManageVacation">请假管理</a></li>
            </ul>
        </div>
        <div id="bottomDiv">
            <div class="detail">
                <div class="inner03">
                    <div id="tabCot_product" class="zhutitab">
                        <div class="tabContainer">
                            <ul class="tabHead" id="tabCot_product-li-currentBtn-">
                                <li class="currentBtn">
                                    <a href="javascript:void(0)" title="请假记录" rel="1">请假记录</a>
                                    <span class="grey_numb" id="vacationCountTwo"></span>
                                </li>
                                <li>
                                    <a href="tosecondManageVacationWait" title="待审批" rel="1">待审批</a>
                                    <span class="grey_numb" id="vacationCountTwoWait"></span>
                                </li>
                            </ul>
                            <div class="clear"></div>
                        </div>
                        <div id="tabCot_product_1" class="tabCot">
                            <table class="tabindex" width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th bgcolor="#f8f8f8" scope="col">员工编号</th>
                                    <th bgcolor="#f8f8f8" scope="col">员工姓名</th>
                                    <th bgcolor="#f8f8f8" scope="col">请假原因</th>
                                    <th bgcolor="#f8f8f8" scope="col">请假时间</th>
                                    <th bgcolor="#f8f8f8" scope="col">请假状态</th>
                                    <th bgcolor="#f8f8f8" scope="col">驳回理由</th>
                                    <th bgcolor="#f8f8f8" scope="col">操作</th>
                                </tr>
                                <c:forEach items="${secondVacationList}" var="slist">
                                    <tr>
                                        <td bgcolor="#FFFFFF">${slist.uid}</td>
                                        <td bgcolor="#FFFFFF">${slist.uname}</td>
                                        <td class="datacol" bgcolor="#FFFFFF">${slist.vreason}</td>
                                        <td bgcolor="#FFFFFF">${slist.vstime}至${slist.vltime}</td>
                                        <td class="waitcol" bgcolor="#FFFFFF">${slist.vstatus}</td>
                                        <td class="waitcol" bgcolor="#FFFFFF">${slist.examReason}</td>
                                        <td class="czcol" bgcolor="#FFFFFF">
                                            <a onclick="passApply(${slist.vid}, ${user.uid})">通过</a>&emsp;
                                            <a title="驳回" rel="1" data-toggle="modal" data-target="#refuseModal" onclick="addVid(${slist.vid})">驳回</a>&emsp;
                                            <!-- 模态框（Modal） -->
                                            <div class="modal fade" id="refuseModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                                &times;
                                                            </button>
                                                            <h4 class="modal-title" id="refuseModalLabel">驳回申请</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <input type="hidden" id="vid" />
                                                            <label style="margin-right: 15px; vertical-align: top;">驳回原因：</label>
                                                            <textarea rows="3" cols="30" id="examReason" style="resize: none;"></textarea><br>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                                                            <button type="button" class="btn btn-primary" onclick="refuseApply($('#vid').val(), ${user.uid}, $('#examReason').val())">提交</button>
                                                        </div>
                                                    </div><!-- /.modal-content -->
                                                </div><!-- /.modal -->
                                            </div>
                                            <a title="员工信息" rel="1" data-toggle="modal" data-target="#employInfoModal">员工信息</a>
                                            <!-- 模态框（Modal） -->
                                            <div class="modal fade" id="employInfoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                                &times;
                                                            </button>
                                                            <h4 class="modal-title">员工信息</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <table>
                                                                <tr>
                                                                    <td><span style="color: black; font-size: 18px;">姓名：</span></td>
                                                                    <td><span style="color: #5cb85c; font-size: 18px;">${slist.uname}</span></td>
                                                                    <td><span style="color: black; font-size: 18px;">性别：</span></td>
                                                                    <td><span style="color: #5cb85c; font-size: 18px;">${slist.usex}</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><span style="color: black; font-size: 18px;">年龄：</span></td>
                                                                    <td><span style="color: #5cb85c; font-size: 18px;">${slist.uage}</span></td>
                                                                    <td><span style="color: black; font-size: 18px;">学历：</span></td>
                                                                    <td><span style="color: #5cb85c; font-size: 18px;">${slist.uedu}</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><span style="color: black; font-size: 18px;">部门ID：</span></td>
                                                                    <td><span style="color: #5cb85c; font-size: 18px;">${slist.udeptid}</span></td>
                                                                    <td><span style="color: black; font-size: 18px;">职位ID：</span></td>
                                                                    <td><span style="color: #5cb85c; font-size: 18px;">${slist.upostid}</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><span style="color: black; font-size: 18px;">入职时间：</span></td>
                                                                    <td><span style="color: #5cb85c; font-size: 18px;">${slist.uintime}</span></td>
                                                                    <td><span style="color: black; font-size: 18px;">联系电话：</span></td>
                                                                    <td><span style="color: #5cb85c; font-size: 18px;">${slist.umobile}</span></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                                                        </div>
                                                    </div><!-- /.modal-content -->
                                                </div><!-- /.modal -->
                                            </div>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </table>
                            <div class="fanye">
                                <p class="fytip">Showing 1 to 10 of 12 entries</p>
                                <div class="yem">
                                    <ul>
                                        <li><a href="#">First</a></li>
                                        <li><a href="#">&lt;</a></li>
                                        <li class="sellify"><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">&gt;</a></li>
                                        <li><a href="#">Last</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div id="tabCot_product_2" class="tabCot"  style="display: none;"> 2222222222 </div>
                        <div id="tabCot_product_3" class="tabCot"  style="display: none;"> 3333333333 </div>
                        <script language="JavaScript" type="text/javascript" src="../js/tab.js"></script>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 模态框（Modal） -->
    <div class="modal fade" id="vacationApplyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="myModalLabel">请假申请</h4>
                </div>
                <div class="modal-body">
                    <label style="margin-right: 15px; vertical-align: top;">请假原因：</label>
                    <textarea rows="3" cols="30" id="vreason" style="resize: none;"></textarea><br><hr>
                    <label style="margin-right: 15px">请假时间：</label>
                    <input type="date" value="2020-04-07" id="vstime" /><label>&emsp;至&emsp;</label><input type="date" value="2020-04-07" id="vltime" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" onclick="addVacation()">提交</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal -->
    </div>
</div>
<div class="footer"></div>
</body>
</html>
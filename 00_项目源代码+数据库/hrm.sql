/*
Navicat MySQL Data Transfer

Source Server         : database
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : hrm

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2020-04-14 19:04:59
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `hr_dept`
-- ----------------------------
DROP TABLE IF EXISTS `hr_dept`;
CREATE TABLE `hr_dept` (
  `deptid` int(11) NOT NULL AUTO_INCREMENT,
  `dname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`deptid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of hr_dept
-- ----------------------------
INSERT INTO `hr_dept` VALUES ('1', '人事部');
INSERT INTO `hr_dept` VALUES ('2', '技术部');
INSERT INTO `hr_dept` VALUES ('3', '财务部');
INSERT INTO `hr_dept` VALUES ('4', '营销部');

-- ----------------------------
-- Table structure for `hr_moverecord`
-- ----------------------------
DROP TABLE IF EXISTS `hr_moverecord`;
CREATE TABLE `hr_moverecord` (
  `rid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `uname` varchar(255) DEFAULT NULL,
  `odeptid` int(11) DEFAULT NULL,
  `opoisid` int(11) DEFAULT NULL,
  `ndeptid` int(11) DEFAULT NULL,
  `npoisid` int(11) DEFAULT NULL,
  `changetime` date DEFAULT NULL,
  PRIMARY KEY (`rid`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of hr_moverecord
-- ----------------------------
INSERT INTO `hr_moverecord` VALUES ('2', '3', 'Amy', '1', '1', '3', '1', '2020-04-02');
INSERT INTO `hr_moverecord` VALUES ('7', '5', '张三', '1', '1', '2', '2', '2020-04-02');
INSERT INTO `hr_moverecord` VALUES ('19', '4', 'Bob', '2', '1', '2', '2', '2020-04-07');
INSERT INTO `hr_moverecord` VALUES ('20', '6', 'Lili', '1', '2', '1', '3', '2020-04-03');
INSERT INTO `hr_moverecord` VALUES ('21', '7', 'Ccc', '3', '1', '3', '3', '2020-04-08');
INSERT INTO `hr_moverecord` VALUES ('23', '8', 'Smith', '2', '1', '2', '3', '2020-04-03');
INSERT INTO `hr_moverecord` VALUES ('28', '9', 'dududu', '2', '1', '2', '3', '2020-04-03');
INSERT INTO `hr_moverecord` VALUES ('29', '1', 'cindy', '1', '2', '1', '3', '2020-04-03');

-- ----------------------------
-- Table structure for `hr_postition`
-- ----------------------------
DROP TABLE IF EXISTS `hr_postition`;
CREATE TABLE `hr_postition` (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `pname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of hr_postition
-- ----------------------------
INSERT INTO `hr_postition` VALUES ('1', '普通员工');
INSERT INTO `hr_postition` VALUES ('2', '部门经理');
INSERT INTO `hr_postition` VALUES ('3', '总经理');

-- ----------------------------
-- Table structure for `hr_user`
-- ----------------------------
DROP TABLE IF EXISTS `hr_user`;
CREATE TABLE `hr_user` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `uname` varchar(255) DEFAULT NULL,
  `upwd` varchar(255) DEFAULT NULL,
  `usex` varchar(255) DEFAULT NULL,
  `uage` int(11) DEFAULT NULL,
  `uedu` varchar(255) DEFAULT NULL,
  `udeptid` int(11) DEFAULT NULL,
  `upostid` int(11) DEFAULT NULL,
  `umobile` varchar(255) DEFAULT NULL,
  `uintime` date DEFAULT NULL,
  `uemail` varchar(255) DEFAULT NULL,
  `ustatus` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of hr_user
-- ----------------------------
INSERT INTO `hr_user` VALUES ('1', 'cindy', '123', '女', '23', '本科', '1', '3', '12323452345', '2020-04-02', '123212321@qq.com', '在职');
INSERT INTO `hr_user` VALUES ('3', 'Amy', '123', '女', '23', '本科', '1', '1', '13234238885', '2020-01-02', '1243423112@qq.com', '在职');
INSERT INTO `hr_user` VALUES ('4', 'Bob', '123', '男', '25', '本科', '2', '1', '12323452346', '2020-02-11', '123212333@qq.com', '在职');
INSERT INTO `hr_user` VALUES ('5', '张三', '123', '男', '32', '大专', '1', '2', '13221421212', '2020-01-02', '174332324@qq.com', '在职');
INSERT INTO `hr_user` VALUES ('6', 'Lili', '123', '女', '30', '本科', '1', '2', '13234238895', '2020-01-02', '12433423612@qq.com', '在职');
INSERT INTO `hr_user` VALUES ('7', 'Ccc', '123', '女', '35', '硕士', '3', '3', '13234230005', '2012-01-02', '12433426622@qq.com', '在职');
INSERT INTO `hr_user` VALUES ('8', 'Smith', '123', '男', '35', '硕士', '2', '3', '13234235555', '2002-01-01', '12433422233@qq.com', '在职');
INSERT INTO `hr_user` VALUES ('9', 'dududu', '123', '女', '22', '大专', '2', '3', '13121212323', '2020-01-02', '1243345454@qq.com', '在职');

-- ----------------------------
-- Table structure for `hr_vacation`
-- ----------------------------
DROP TABLE IF EXISTS `hr_vacation`;
CREATE TABLE `hr_vacation` (
  `vid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `vreason` varchar(255) DEFAULT NULL,
  `vstime` date DEFAULT NULL,
  `vltime` date DEFAULT NULL,
  `vstatus` varchar(255) DEFAULT NULL,
  `examreason` varchar(255) DEFAULT NULL,
  `examuser` int(11) DEFAULT NULL,
  PRIMARY KEY (`vid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of hr_vacation
-- ----------------------------
INSERT INTO `hr_vacation` VALUES ('1', '1', '家人结婚', '2020-04-07', '2020-04-07', '待审批', '无', '0');
INSERT INTO `hr_vacation` VALUES ('2', '3', '病假', '2020-04-04', '2020-04-05', '待审批', '无', '0');
